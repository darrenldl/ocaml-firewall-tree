module M = Firewall_tree.Make (Firewall_tree.Mock_tree_base)
module T = Firewall_tree_test.Make (M)

let () =
  T.Tree_base_test.run_tests_qcheck ();
  T.Tree_base_extended_test.run_tests_qcheck ();
  (* wait a bit for bisect_ppx *)
  Unix.sleep 5
