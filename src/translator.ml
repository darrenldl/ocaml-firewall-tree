module type S = sig
  type pdu

  type ipv4_addr

  type conn_tracker

  module Side_A_to_random_src_port_side_B : sig
    type translator

    val make :
      ?conn_tracker:conn_tracker
      -> side_A_addr:ipv4_addr
      -> side_B_addr:ipv4_addr
      -> side_B_port_start:int
      -> side_B_port_end_exc:int
      -> int
      -> translator

    val translate_side_A_to_B : translator -> pdu -> pdu option

    val translate_side_B_to_A : translator -> pdu -> pdu option
  end

  module Side_A_to_random_dst_side_B : sig
    type translator

    val make :
      ?conn_tracker:conn_tracker
      -> side_A_addr:ipv4_addr
      -> side_B_addr:ipv4_addr
      -> side_B_port_start:int
      -> side_B_port_end_exc:int
      -> dst_addrs:ipv4_addr array
      -> int
      -> translator

    val translate_side_A_to_B : translator -> pdu -> pdu option

    val translate_side_B_to_A : translator -> pdu -> pdu option
  end
end

module Make
    (B : Tree_base_extended.S)
    (Conn_track : Conn_track.S
     with type ipv4_addr := B.IPv4.ipv4_addr
      and type ipv6_addr := B.IPv6.ipv6_addr
      and type tcp_port := B.tcp_port
      and type udp_port := B.udp_port
      and type pdu := B.PDU.pdu
      and type icmpv4_type := B.ICMPv4.icmpv4_type) :
  S
  with type pdu := B.PDU.pdu
   and type ipv4_addr := B.IPv4.ipv4_addr
   and type conn_tracker := Conn_track.conn_tracker = struct
  include B

  module IPv4_UDP =
    Translator_generic.Make (struct
      include B

      type l3_header = IPv4.ipv4_header

      type l4_header = UDP.udp_header

      type addr = IPv4.ipv4_addr

      let addr_to_byte_string = IPv4.ipv4_addr_to_byte_string

      let byte_string_to_addr = IPv4.byte_string_to_ipv4_addr

      type port = udp_port

      let port_lt = ( < )

      let pdu_to_l3_header = PDU_to.ipv4_header

      let pdu_map_l3_header = PDU_map.ipv4_header

      let l3_header_to_src_addr = IPv4.ipv4_header_to_src_addr

      let l3_header_to_dst_addr = IPv4.ipv4_header_to_dst_addr

      let update_l3_header ~src_addr ~dst_addr header =
        B.IPv4.update_ipv4_header ~src_addr ~dst_addr header

      let pdu_to_l4_header = PDU_to.udp_header

      let pdu_map_l4_header = PDU_map.udp_header

      let l4_header_to_src_port = UDP.udp_header_to_src_port

      let l4_header_to_dst_port = UDP.udp_header_to_dst_port

      let update_l4_header ~src_port ~dst_port header =
        B.UDP.update_udp_header ~src_port ~dst_port header
    end)
      (Conn_track)

  module IPv4_TCP =
    Translator_generic.Make (struct
      include B

      type l3_header = IPv4.ipv4_header

      type l4_header = TCP.tcp_header

      type addr = IPv4.ipv4_addr

      let addr_to_byte_string = IPv4.ipv4_addr_to_byte_string

      let byte_string_to_addr = IPv4.byte_string_to_ipv4_addr

      type port = tcp_port

      let port_lt = ( < )

      let pdu_to_l3_header = B.PDU_to.ipv4_header

      let pdu_map_l3_header = B.PDU_map.ipv4_header

      let l3_header_to_src_addr = IPv4.ipv4_header_to_src_addr

      let l3_header_to_dst_addr = IPv4.ipv4_header_to_dst_addr

      let update_l3_header ~src_addr ~dst_addr header =
        B.IPv4.update_ipv4_header ~src_addr ~dst_addr header

      let pdu_to_l4_header = B.PDU_to.tcp_header

      let pdu_map_l4_header = B.PDU_map.tcp_header

      let l4_header_to_src_port = TCP.tcp_header_to_src_port

      let l4_header_to_dst_port = TCP.tcp_header_to_dst_port

      let update_l4_header ~src_port ~dst_port header =
        B.TCP.update_tcp_header ~src_port ~dst_port header
    end)
      (Conn_track)

  module Side_A_to_random_src_port_side_B = struct
    module IPv4_TCP = IPv4_TCP.Side_A_to_random_src_port_side_B
    module IPv4_UDP = IPv4_UDP.Side_A_to_random_src_port_side_B
    open IPv4
    open PDU

    type translator =
      { ipv4_tcp_translator : IPv4_TCP.translator
      ; ipv4_udp_translator : IPv4_UDP.translator }

    let make ?(conn_tracker : Conn_track.conn_tracker option)
        ~(side_A_addr : ipv4_addr) ~(side_B_addr : ipv4_addr)
        ~(side_B_port_start : int) ~(side_B_port_end_exc : int)
        (max_conn : int) : translator =
      assert (side_B_port_start <= side_B_port_end_exc);
      let init_size = max_conn / 2 in
      let timeout_ms = 30_000L in
      let conn_tracker =
        match conn_tracker with
        | Some c ->
          c
        | None ->
          Conn_track.make ~max_conn ~init_size ~timeout_ms
      in
      { ipv4_tcp_translator =
          IPv4_TCP.make ~max_conn ~side_A_addr ~side_B_addr ~side_B_port_start
            ~side_B_port_end_exc conn_tracker
      ; ipv4_udp_translator =
          IPv4_UDP.make ~max_conn ~side_A_addr ~side_B_addr ~side_B_port_start
            ~side_B_port_end_exc conn_tracker }

    let translate_side_A_to_B (t : translator) (pdu : pdu) : pdu option =
      match
        ( IPv4_TCP.translate_side_A_to_B t.ipv4_tcp_translator pdu
        , IPv4_UDP.translate_side_A_to_B t.ipv4_udp_translator pdu )
      with
      | None, None ->
        None
      | Some p, None ->
        Some p
      | None, Some p ->
        Some p
      | Some _, Some _ ->
        failwith "Unexpected case"

    let translate_side_B_to_A (t : translator) (pdu : pdu) : pdu option =
      match
        ( IPv4_TCP.translate_side_B_to_A t.ipv4_tcp_translator pdu
        , IPv4_UDP.translate_side_B_to_A t.ipv4_udp_translator pdu )
      with
      | None, None ->
        None
      | Some p, None ->
        Some p
      | None, Some p ->
        Some p
      | Some _, Some _ ->
        failwith "Unexpected case"
  end

  module Side_A_to_random_dst_side_B = struct
    module IPv4_TCP = IPv4_TCP.Side_A_to_random_dst_side_B
    module IPv4_UDP = IPv4_UDP.Side_A_to_random_dst_side_B
    open IPv4
    open PDU

    type translator =
      { ipv4_tcp_translator : IPv4_TCP.translator
      ; ipv4_udp_translator : IPv4_UDP.translator }

    let make ?(conn_tracker : Conn_track.conn_tracker option)
        ~(side_A_addr : ipv4_addr) ~(side_B_addr : ipv4_addr)
        ~(side_B_port_start : int) ~(side_B_port_end_exc : int)
        ~(dst_addrs : ipv4_addr array) (max_conn : int) : translator =
      assert (side_B_port_start <= side_B_port_end_exc);
      let init_size = max_conn / 2 in
      let timeout_ms = 30_000L in
      let conn_tracker =
        match conn_tracker with
        | Some c ->
          c
        | None ->
          Conn_track.make ~max_conn ~init_size ~timeout_ms
      in
      { ipv4_tcp_translator =
          IPv4_TCP.make ~max_conn ~side_A_addr ~side_B_addr ~side_B_port_start
            ~side_B_port_end_exc ~dst_addrs conn_tracker
      ; ipv4_udp_translator =
          IPv4_UDP.make ~max_conn ~side_A_addr ~side_B_addr ~side_B_port_start
            ~side_B_port_end_exc ~dst_addrs conn_tracker }

    let translate_side_A_to_B (t : translator) (pdu : pdu) : pdu option =
      match
        ( IPv4_TCP.translate_side_A_to_B t.ipv4_tcp_translator pdu
        , IPv4_UDP.translate_side_A_to_B t.ipv4_udp_translator pdu )
      with
      | None, None ->
        None
      | Some p, None ->
        Some p
      | None, Some p ->
        Some p
      | Some _, Some _ ->
        failwith "Unexpected case"

    let translate_side_B_to_A (t : translator) (pdu : pdu) : pdu option =
      match
        ( IPv4_TCP.translate_side_B_to_A t.ipv4_tcp_translator pdu
        , IPv4_UDP.translate_side_B_to_A t.ipv4_udp_translator pdu )
      with
      | None, None ->
        None
      | Some p, None ->
        Some p
      | None, Some p ->
        Some p
      | Some _, Some _ ->
        failwith "Unexpected case"
  end
end
