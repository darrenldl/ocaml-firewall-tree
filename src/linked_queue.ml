type 'a t = {mutable queue : 'a Queue.t}

let create () : 'a t = {queue = Queue.create ()}

let filter_inplace (f : 'a -> bool) (t : 'a t) : unit =
  let res = Queue.create () in
  Queue.iter (fun v -> if f v then Queue.push v res) t.queue;
  Queue.clear t.queue;
  t.queue <- res

let push (v : 'a) (t : 'a t) : unit = Queue.push v t.queue

let push_all (vs : 'a list) (t : 'a t) : unit =
  List.iter (fun v -> push v t) vs

let pop (t : 'a t) : 'a = Queue.pop t.queue

let pop_opt (t : 'a t) : 'a option =
  if Queue.is_empty t.queue then None else Some (Queue.pop t.queue)

let to_list (t : 'a t) : 'a list =
  List.rev (Queue.fold (fun acc v -> v :: acc) [] t.queue)

let iter (f : 'a -> unit) (t : 'a t) : unit = Queue.iter f t.queue
