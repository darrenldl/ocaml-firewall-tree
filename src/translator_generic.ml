module type B = sig
  include Tree_base_extended.S

  open PDU

  type l3_header

  type l4_header

  type addr

  val addr_to_byte_string : addr -> string

  val byte_string_to_addr : string -> addr

  type port = int

  val port_lt : port -> port -> bool

  val pdu_to_l3_header : pdu -> l3_header option

  val pdu_map_l3_header : (l3_header -> l3_header) -> pdu -> pdu

  val l3_header_to_src_addr : l3_header -> addr

  val l3_header_to_dst_addr : l3_header -> addr

  val update_l3_header :
    src_addr:addr -> dst_addr:addr -> l3_header -> l3_header

  val pdu_to_l4_header : pdu -> l4_header option

  val pdu_map_l4_header : (l4_header -> l4_header) -> pdu -> pdu

  val l4_header_to_src_port : l4_header -> port

  val l4_header_to_dst_port : l4_header -> port

  val update_l4_header :
    src_port:port -> dst_port:port -> l4_header -> l4_header
end

module type S = sig
  type addr

  type port

  type pdu

  type conn_tracker

  module Side_A_to_random_src_port_side_B : sig
    type translator

    val make :
      max_conn:int
      -> side_A_addr:addr
      -> side_B_addr:addr
      -> side_B_port_start:port
      -> side_B_port_end_exc:port
      -> conn_tracker
      -> translator

    val translate_side_A_to_B : translator -> pdu -> pdu option

    val translate_side_B_to_A : translator -> pdu -> pdu option
  end

  module Side_A_to_random_dst_side_B : sig
    type translator

    val make :
      max_conn:int
      -> side_A_addr:addr
      -> side_B_addr:addr
      -> side_B_port_start:port
      -> side_B_port_end_exc:port
      -> dst_addrs:addr array
      -> conn_tracker
      -> translator

    val translate_side_A_to_B : translator -> pdu -> pdu option

    val translate_side_B_to_A : translator -> pdu -> pdu option
  end
end

module Make
    (B : B)
    (Conn_track : Conn_track.S
     with type ipv4_addr := B.IPv4.ipv4_addr
      and type ipv6_addr := B.IPv6.ipv6_addr
      and type tcp_port := B.tcp_port
      and type udp_port := B.udp_port
      and type pdu := B.PDU.pdu
      and type icmpv4_type := B.ICMPv4.icmpv4_type) :
  S
  with type addr := B.addr
   and type port := B.port
   and type pdu := B.PDU.pdu
   and type conn_tracker := Conn_track.conn_tracker = struct
  include B
  open PDU

  type conn_tracker = Conn_track.conn_tracker

  let update_l3_l4_header ~(src_addr : addr) ~(dst_addr : addr)
      ~(src_port : port) ~(dst_port : port) (pdu : pdu) : pdu =
    pdu
    |> pdu_map_l3_header (update_l3_header ~src_addr ~dst_addr)
    |> pdu_map_l4_header (update_l4_header ~src_port ~dst_port)

  module Side_A_to_random_src_port_side_B = struct
    type translator =
      { side_A_addr : string
      ; side_B_addr : string
      ; side_B_port_start : port
      ; side_B_port_end_exc : port
      ; side_B_port_selector : unit -> port option
      ; conn_tracker : conn_tracker
      ; addr_port_to_side_A_port : (string * port, port) Lookup_table.t
      ; side_A_port_to_addr_port : (port, string * port) Lookup_table.t
      ; side_A_port_to_side_B_port : (port, port) Lookup_table.t
      ; side_B_port_to_side_A_port : (port, port) Lookup_table.t
      ; addr_port_to_side_B_port : (string * port, port) Lookup_table.t
      ; side_B_port_to_addr_port : (port, string * port) Lookup_table.t }

    let make ~(max_conn : int) ~(side_A_addr : addr) ~(side_B_addr : addr)
        ~(side_B_port_start : port) ~(side_B_port_end_exc : port)
        (conn_tracker : Conn_track.conn_tracker) : translator =
      assert (port_lt side_B_port_start side_B_port_end_exc);
      let max_size = max_conn in
      let init_size = max_size / 2 in
      let timeout_ms = Some 30_000L in
      let side_A_addr = addr_to_byte_string side_A_addr in
      let side_B_addr = addr_to_byte_string side_B_addr in
      let addr_port_to_side_A_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_A_port_to_addr_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_A_port_to_side_B_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_B_port_to_side_A_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_B_port_to_addr_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let addr_port_to_side_B_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_B_port_selector =
        Misc_utils.make_random_generator_exc_opt
          ~pred:(fun x ->
              match
                ( Lookup_table.find_opt side_B_port_to_addr_port x
                , Lookup_table.find_opt side_B_port_to_side_A_port x )
              with
              | None, None ->
                true
              | _ ->
                false )
          ~from:side_B_port_start side_B_port_end_exc
      in
      { side_A_addr
      ; side_B_addr
      ; side_B_port_start
      ; side_B_port_end_exc
      ; side_B_port_selector
      ; conn_tracker
      ; addr_port_to_side_A_port
      ; side_A_port_to_addr_port
      ; side_A_port_to_side_B_port
      ; side_B_port_to_side_A_port
      ; side_B_port_to_addr_port
      ; addr_port_to_side_B_port }

    let add_mapping_between_addr_port_and_side_A_port (t : translator)
        ~(addr : addr) ~(port : port) ~(side_A_port : port) : unit =
      let addr = addr_to_byte_string addr in
      Lookup_table.add_bidirectional_mapping t.addr_port_to_side_A_port
        t.side_A_port_to_addr_port (addr, port) side_A_port

    let add_mapping_between_side_A_port_and_side_B_port (t : translator)
        ~(side_A_port : port) ~(side_B_port : port) : unit =
      Lookup_table.add_bidirectional_mapping t.side_A_port_to_side_B_port
        t.side_B_port_to_side_A_port side_A_port side_B_port

    let add_mapping_between_side_B_port_and_addr_port (t : translator)
        ~(side_B_port : port) ~(addr : addr) ~(port : port) : unit =
      let addr = addr_to_byte_string addr in
      Lookup_table.add_bidirectional_mapping t.side_B_port_to_addr_port
        t.addr_port_to_side_B_port side_B_port (addr, port)

    let evict_timed_out (t : translator) : unit =
      Lookup_table.evict_timed_out t.addr_port_to_side_A_port;
      Lookup_table.evict_timed_out t.side_A_port_to_addr_port;
      Lookup_table.evict_timed_out t.side_A_port_to_side_B_port;
      Lookup_table.evict_timed_out t.side_B_port_to_side_A_port;
      Lookup_table.evict_timed_out t.side_B_port_to_addr_port;
      Lookup_table.evict_timed_out t.addr_port_to_side_B_port

    let translate_side_A_to_B (t : translator) (pdu : pdu) : pdu option =
      evict_timed_out t;
      match (pdu_to_l3_header pdu, pdu_to_l4_header pdu) with
      | None, None | None, Some _ | Some _, None ->
        None
      | Some l3_header, Some l4_header -> (
          let src_addr = l3_header_to_src_addr l3_header in
          let dst_addr = l3_header_to_dst_addr l3_header in
          let src_port = l4_header_to_src_port l4_header in
          let dst_port = l4_header_to_dst_port l4_header in
          let src_addr_str = addr_to_byte_string src_addr in
          let dst_addr_str = addr_to_byte_string dst_addr in
          (* A to B

               run pdu through tracker before and after translation

               introduce new mapping if needed
          *)
          match
            Lookup_table.find_opt3 t.addr_port_to_side_A_port
              t.side_A_port_to_side_B_port t.side_B_port_to_addr_port
              (src_addr_str, src_port)
          with
          | Some (_side_A_port, side_B_port, (target_addr, target_port))
            when target_addr = dst_addr_str && target_port = dst_port ->
            let src_addr = t.side_B_addr |> byte_string_to_addr in
            let src_port = side_B_port in
            Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
            let pdu =
              update_l3_l4_header ~src_addr ~dst_addr ~src_port ~dst_port pdu
            in
            Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
            Some pdu
          | _ -> (
              (* add new mapping *)
              match t.side_B_port_selector () with
              | None ->
                None
              | Some side_B_port ->
                let side_A_port = dst_port in
                add_mapping_between_addr_port_and_side_A_port t ~addr:src_addr
                  ~port:src_port ~side_A_port;
                add_mapping_between_side_A_port_and_side_B_port t ~side_A_port
                  ~side_B_port;
                add_mapping_between_side_B_port_and_addr_port t ~side_B_port
                  ~addr:dst_addr ~port:dst_port;
                let src_addr = t.side_B_addr |> byte_string_to_addr in
                let src_port = side_B_port in
                Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
                let pdu =
                  update_l3_l4_header ~src_addr ~dst_addr ~src_port ~dst_port
                    pdu
                in
                Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
                Some pdu ) )

    let translate_side_B_to_A (t : translator) (pdu : pdu) : pdu option =
      evict_timed_out t;
      match (pdu_to_l3_header pdu, pdu_to_l4_header pdu) with
      | None, None | None, Some _ | Some _, None ->
        None
      | Some l3_header, Some l4_header -> (
          let src_addr = l3_header_to_src_addr l3_header in
          let src_port = l4_header_to_src_port l4_header in
          let dst_port = l4_header_to_dst_port l4_header in
          let src_addr_str = addr_to_byte_string src_addr in
          (* B to A

               run pdu through tracker first before translation, reject new
               this is done by lookup_conn_state already

               do not introduce new mapping

               run pdu through tracker after translation
          *)
          match
            Conn_track.lookup_conn_state ~reject_new:true t.conn_tracker pdu
          with
          | Established -> (
              match
                Lookup_table.find_opt3 t.addr_port_to_side_B_port
                  t.side_B_port_to_side_A_port t.side_A_port_to_addr_port
                  (src_addr_str, src_port)
              with
              | Some (side_B_port, side_A_port, (target_addr, target_port))
                when side_B_port = dst_port ->
                let dst_addr = target_addr |> byte_string_to_addr in
                let dst_port = target_port in
                let src_port = side_A_port in
                let pdu =
                  update_l3_l4_header ~src_addr ~dst_addr ~src_port ~dst_port
                    pdu
                in
                Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
                Some pdu
              | _ ->
                None )
          | _ ->
            None )
  end

  module Side_A_to_random_dst_side_B = struct
    type translator =
      { side_A_addr : addr
      ; side_B_addr : addr
      ; side_B_port_start : port
      ; side_B_port_end_exc : port
      ; side_B_port_selector : unit -> port option
      ; dst_addrs : addr array
      ; dst_addr_selector : unit -> int
      ; conn_tracker : conn_tracker
      ; addr_port_to_side_A_port : (addr * port, port) Lookup_table.t
      ; side_A_port_to_addr_port : (port, addr * port) Lookup_table.t
      ; side_A_port_to_side_B_port : (port, port) Lookup_table.t
      ; side_B_port_to_side_A_port : (port, port) Lookup_table.t
      ; side_B_port_to_addr_port : (port, addr * port) Lookup_table.t
      ; addr_port_to_side_B_port : (addr * port, port) Lookup_table.t }

    let make ~(max_conn : int) ~(side_A_addr : addr) ~(side_B_addr : addr)
        ~(side_B_port_start : port) ~(side_B_port_end_exc : port)
        ~(dst_addrs : addr array) (conn_tracker : Conn_track.conn_tracker) :
      translator =
      assert (port_lt side_B_port_start side_B_port_end_exc);
      let max_size = max_conn in
      let init_size = max_size / 2 in
      let timeout_ms = Some 30_000L in
      let addr_port_to_side_A_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_A_port_to_addr_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_A_port_to_side_B_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_B_port_to_side_A_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_B_port_to_addr_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let addr_port_to_side_B_port =
        Lookup_table.create ~max_size ~init_size ~timeout_ms
      in
      let side_B_port_selector =
        Misc_utils.make_random_generator_exc_opt
          ~pred:(fun x ->
              match
                ( Lookup_table.find_opt side_B_port_to_addr_port x
                , Lookup_table.find_opt side_B_port_to_side_A_port x )
              with
              | None, None ->
                true
              | _ ->
                false )
          ~from:side_B_port_start side_B_port_end_exc
      in
      let dst_addr_selector =
        Misc_utils.make_random_generator_exc_dynamic_ub ~from:0 (fun () ->
            Array.length dst_addrs )
      in
      { side_A_addr
      ; side_B_addr
      ; side_B_port_start
      ; side_B_port_end_exc
      ; side_B_port_selector
      ; dst_addrs
      ; dst_addr_selector
      ; conn_tracker
      ; addr_port_to_side_A_port
      ; side_A_port_to_addr_port
      ; side_A_port_to_side_B_port
      ; side_B_port_to_side_A_port
      ; side_B_port_to_addr_port
      ; addr_port_to_side_B_port }

    let add_mapping_between_addr_port_and_side_A_port (t : translator)
        ~(addr : addr) ~(port : port) ~(side_A_port : port) : unit =
      Lookup_table.add_bidirectional_mapping t.addr_port_to_side_A_port
        t.side_A_port_to_addr_port (addr, port) side_A_port

    let add_mapping_between_side_A_port_and_side_B_port (t : translator)
        ~(side_A_port : port) ~(side_B_port : port) : unit =
      Lookup_table.add_bidirectional_mapping t.side_A_port_to_side_B_port
        t.side_B_port_to_side_A_port side_A_port side_B_port

    let add_mapping_between_side_B_port_and_addr_port (t : translator)
        ~(side_B_port : port) ~(addr : addr) ~(port : port) : unit =
      Lookup_table.add_bidirectional_mapping t.side_B_port_to_addr_port
        t.addr_port_to_side_B_port side_B_port (addr, port)

    let evict_timed_out (t : translator) : unit =
      Lookup_table.evict_timed_out t.addr_port_to_side_A_port;
      Lookup_table.evict_timed_out t.side_A_port_to_addr_port;
      Lookup_table.evict_timed_out t.side_A_port_to_side_B_port;
      Lookup_table.evict_timed_out t.side_B_port_to_side_A_port;
      Lookup_table.evict_timed_out t.side_B_port_to_addr_port;
      Lookup_table.evict_timed_out t.addr_port_to_side_B_port

    let translate_side_A_to_B (t : translator) (pdu : pdu) : pdu option =
      evict_timed_out t;
      match (pdu_to_l3_header pdu, pdu_to_l4_header pdu) with
      | None, None | None, Some _ | Some _, None ->
        None
      | Some l3_header, Some l4_header -> (
          let src_addr = l3_header_to_src_addr l3_header in
          let dst_addr = l3_header_to_dst_addr l3_header in
          let src_port = l4_header_to_src_port l4_header in
          let dst_port = l4_header_to_dst_port l4_header in
          (* A to B

               run pdu through tracker before and after translation

               introduce new mapping if needed
          *)
          match
            Lookup_table.find_opt3 t.addr_port_to_side_A_port
              t.side_A_port_to_side_B_port t.side_B_port_to_addr_port
              (src_addr, src_port)
          with
          | Some (side_A_port, side_B_port, (target_addr, target_port))
            when side_A_port = dst_port && t.side_A_addr = dst_addr
                 && target_port = dst_port ->
            let src_addr = t.side_B_addr in
            let src_port = side_B_port in
            let dst_addr_choice = t.dst_addr_selector () in
            let dst_addr = t.dst_addrs.(dst_addr_choice) in
            Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
            let pdu =
              update_l3_l4_header ~src_addr ~dst_addr ~src_port ~dst_port pdu
            in
            Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
            Some pdu
          | _ -> (
              (* add new mapping *)
              match t.side_B_port_selector () with
              | None ->
                None
              | Some side_B_port ->
                let side_A_port = dst_port in
                add_mapping_between_addr_port_and_side_A_port t ~addr:src_addr
                  ~port:src_port ~side_A_port;
                add_mapping_between_side_A_port_and_side_B_port t ~side_A_port
                  ~side_B_port;
                let src_addr = t.side_B_addr in
                let src_port = side_B_port in
                let dst_addr_choice = t.dst_addr_selector () in
                let dst_addr = t.dst_addrs.(dst_addr_choice) in
                add_mapping_between_side_B_port_and_addr_port t ~side_B_port
                  ~addr:dst_addr ~port:dst_port;
                Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
                let pdu =
                  update_l3_l4_header ~src_addr ~dst_addr ~src_port ~dst_port
                    pdu
                in
                Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
                Some pdu ) )

    let translate_side_B_to_A (t : translator) (pdu : pdu) : pdu option =
      evict_timed_out t;
      match (pdu_to_l3_header pdu, pdu_to_l4_header pdu) with
      | None, None | None, Some _ | Some _, None ->
        None
      | Some l3_header, Some l4_header -> (
          let src_addr = l3_header_to_src_addr l3_header in
          let src_port = l4_header_to_src_port l4_header in
          let dst_port = l4_header_to_dst_port l4_header in
          (* B to A

               run pdu through tracker first before translation, reject new
               this is done by lookup_conn_state already

               do not introduce new mapping

               run pdu through tracker after translation
          *)
          match
            Conn_track.lookup_conn_state ~reject_new:true t.conn_tracker pdu
          with
          | Established -> (
              match
                Lookup_table.find_opt3 t.addr_port_to_side_B_port
                  t.side_B_port_to_side_A_port t.side_A_port_to_addr_port
                  (src_addr, src_port)
              with
              | Some (side_B_port, side_A_port, (target_addr, target_port))
                when side_B_port = dst_port ->
                let dst_addr = target_addr in
                let dst_port = target_port in
                let src_addr = t.side_A_addr in
                let src_port = side_A_port in
                let pdu =
                  update_l3_l4_header ~src_addr ~dst_addr ~src_port ~dst_port
                    pdu
                in
                Conn_track.pass_pdu_through_tracker t.conn_tracker pdu;
                Some pdu
              | _ ->
                None )
          | _ ->
            None )
  end
end
