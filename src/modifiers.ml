module type S = sig
  type decision_tree_branch

  type conn_tracker

  type ipv4_addr

  type ipv6_addr

  type translator_branches =
    { translate_side_A_to_B : decision_tree_branch -> decision_tree_branch
    ; translate_side_B_to_A : decision_tree_branch -> decision_tree_branch }

  val translate_ipv4_side_A_to_random_src_port_side_B :
    ?conn_tracker:conn_tracker
    -> side_A_addr:ipv4_addr
    -> side_B_addr:ipv4_addr
    -> side_B_port_start:int
    -> side_B_port_end_exc:int
    -> max_conn:int
    -> translator_branches

  val translate_ipv4_side_A_to_random_dst_side_B :
    ?conn_tracker:conn_tracker
    -> side_A_addr:ipv4_addr
    -> side_B_addr:ipv4_addr
    -> side_B_port_start:int
    -> side_B_port_end_exc:int
    -> dst_addrs:ipv4_addr array
    -> max_conn:int
    -> translator_branches
end

module Make (B : Tree.S) :
  S
  with type decision_tree_branch := B.decision_tree_branch
   and type conn_tracker := B.Conn_track.conn_tracker
   and type ipv4_addr := B.IPv4.ipv4_addr
   and type ipv6_addr := B.IPv6.ipv6_addr = struct
  include B

  type translator_branches =
    { translate_side_A_to_B : decision_tree_branch -> decision_tree_branch
    ; translate_side_B_to_A : decision_tree_branch -> decision_tree_branch }

  module T1 = Translator.Side_A_to_random_src_port_side_B
  module T2 = Translator.Side_A_to_random_dst_side_B

  let translate_ipv4_side_A_to_random_src_port_side_B
      ?(conn_tracker : Conn_track.conn_tracker option)
      ~(side_A_addr : IPv4.ipv4_addr) ~(side_B_addr : IPv4.ipv4_addr)
      ~(side_B_port_start : int) ~(side_B_port_end_exc : int) ~(max_conn : int)
    : translator_branches =
    let translator =
      match conn_tracker with
      | None ->
        T1.make ~side_A_addr ~side_B_addr ~side_B_port_start
          ~side_B_port_end_exc max_conn
      | Some conn_tracker ->
        T1.make ~conn_tracker ~side_A_addr ~side_B_addr ~side_B_port_start
          ~side_B_port_end_exc max_conn
    in
    let translate_side_A_to_B next =
      let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 pdu =
        T1.translate_side_A_to_B translator pdu
      in
      make_modifier logic_unit next
    in
    let translate_side_B_to_A next =
      let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 pdu =
        T1.translate_side_B_to_A translator pdu
      in
      make_modifier logic_unit next
    in
    {translate_side_A_to_B; translate_side_B_to_A}

  let translate_ipv4_side_A_to_random_dst_side_B
      ?(conn_tracker : Conn_track.conn_tracker option)
      ~(side_A_addr : IPv4.ipv4_addr) ~(side_B_addr : IPv4.ipv4_addr)
      ~(side_B_port_start : int) ~(side_B_port_end_exc : int)
      ~(dst_addrs : IPv4.ipv4_addr array) ~(max_conn : int) :
    translator_branches =
    let translator =
      match conn_tracker with
      | None ->
        T2.make ~side_A_addr ~side_B_addr ~side_B_port_start
          ~side_B_port_end_exc ~dst_addrs max_conn
      | Some conn_tracker ->
        T2.make ~conn_tracker ~side_A_addr ~side_B_addr ~side_B_port_start
          ~side_B_port_end_exc ~dst_addrs max_conn
    in
    let translate_side_A_to_B next =
      let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 pdu =
        T2.translate_side_A_to_B translator pdu
      in
      make_modifier logic_unit next
    in
    let translate_side_B_to_A next =
      let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 pdu =
        T2.translate_side_B_to_A translator pdu
      in
      make_modifier logic_unit next
    in
    {translate_side_A_to_B; translate_side_B_to_A}
end
