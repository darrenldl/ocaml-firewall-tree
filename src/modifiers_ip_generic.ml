module type B = sig
  include Tree.S

  open PDU

  type addr

  type header

  type pkt

  val pdu_to_header : pdu -> header option

  val header_to_src_addr : header -> addr

  val header_to_dst_addr : header -> addr

  val pdu_map_pkt : (pkt -> pkt) -> pdu -> pdu

  val pdu_map_header : (header -> header) -> pdu -> pdu

  val make_header : src_addr:addr -> dst_addr:addr -> header
end

module type S = sig
  type modifier

  type decision_tree_branch

  type conn_tracker

  type addr
end

module Make_ip_generic (B : B) :
  S
  with type modifier := B.modifier
   and type conn_tracker := B.Conn_track.conn_tracker
   and type addr := B.addr = struct
  include B
end
