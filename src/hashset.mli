type 'a t

val create : int -> 'a t

val add : 'a t -> 'a -> unit

val remove : 'a t -> 'a -> unit

val mem : 'a t -> 'a -> bool
