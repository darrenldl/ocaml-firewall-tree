(*$ #use "src/tree_base.cinaps"
  $*)
type decision =
  | Pass
  | Drop

type netif = unit

let cur_time_ms () = Int64.of_float (Sys.time () *. 1000.0)

let id x = x

module Ether = struct
  type ether_addr = string

  type ether_header =
    | Ether of {mutable src_addr : ether_addr; mutable dst_addr : ether_addr}

  type ether_payload_raw = string

  let compare_ether_addr = compare

  let ether_addr_to_byte_string = id

  let byte_string_to_ether_addr = id

  let ether_header_to_src_addr (Ether header) = header.src_addr

  let ether_header_to_dst_addr (Ether header) = header.dst_addr

  let make_dummy_ether_header () =
    Ether
      { src_addr = "\x00\x00\x00\x00\x00\x00"
      ; dst_addr = "\x00\x00\x00\x00\x00\x00" }

  let update_ether_header_ ~src_addr ~dst_addr (Ether header) =
    (match src_addr with None -> () | Some x -> header.src_addr <- x);
    (match dst_addr with None -> () | Some x -> header.dst_addr <- x);
    Ether header

  let update_ether_header_byte_string_ = update_ether_header_

  let ether_payload_raw_to_byte_string = id

  let byte_string_to_ether_payload_raw = id
end

module IPv4 = struct
  type ipv4_addr = string

  type ipv4_header =
    | IPv4 of {mutable src_addr : ipv4_addr; mutable dst_addr : ipv4_addr}

  type ipv4_payload_raw = string

  let compare_ipv4_addr = compare

  let ipv4_addr_to_byte_string = id

  let byte_string_to_ipv4_addr = id

  let ipv4_header_to_src_addr (IPv4 header) = header.src_addr

  let ipv4_header_to_dst_addr (IPv4 header) = header.dst_addr

  let make_dummy_ipv4_header () =
    IPv4 {src_addr = "\xC0\xA8\x00\x01"; dst_addr = "\xC0\xA8\x00\x02"}

  let update_ipv4_header_ ~src_addr ~dst_addr (IPv4 header) =
    (match src_addr with None -> () | Some x -> header.src_addr <- x);
    (match dst_addr with None -> () | Some x -> header.dst_addr <- x);
    IPv4 header

  let update_ipv4_header_byte_string_ = update_ipv4_header_

  let ipv4_payload_raw_to_byte_string = id

  let byte_string_to_ipv4_payload_raw = id
end

module IPv6 = struct
  type ipv6_addr = string

  type ipv6_header =
    | IPv6 of {mutable src_addr : ipv6_addr; mutable dst_addr : ipv6_addr}

  type ipv6_payload_raw = string

  let compare_ipv6_addr = compare

  let ipv6_addr_to_byte_string = id

  let byte_string_to_ipv6_addr = id

  let ipv6_header_to_src_addr (IPv6 header) = header.src_addr

  let ipv6_header_to_dst_addr (IPv6 header) = header.dst_addr

  let make_dummy_ipv6_header () =
    IPv6
      { src_addr =
          "\xC0\xA8\x00\x01\xC0\xA8\x00\x01\xC0\xA8\x00\x01\xC0\xA8\x00\x01"
      ; dst_addr =
          "\xC0\xA8\x00\x01\xC0\xA8\x00\x01\xC0\xA8\x00\x01\xC0\xA8\x00\x01" }

  let update_ipv6_header_ ~src_addr ~dst_addr (IPv6 header) =
    (match src_addr with None -> () | Some x -> header.src_addr <- x);
    (match dst_addr with None -> () | Some x -> header.dst_addr <- x);
    IPv6 header

  let update_ipv6_header_byte_string_ = update_ipv6_header_

  let ipv6_payload_raw_to_byte_string = id

  let byte_string_to_ipv6_payload_raw = id
end

module ICMPv4 = struct
  (*$ print_icmpv4_type_def ()
  *)
  type icmpv4_type =
    | ICMPv4_Echo_reply of {id : string; seq : int}
    | ICMPv4_Destination_unreachable
    | ICMPv4_Source_quench
    | ICMPv4_Redirect
    | ICMPv4_Echo_request of {id : string; seq : int}
    | ICMPv4_Time_exceeded
    | ICMPv4_Parameter_problem
    | ICMPv4_Timestamp_request of {id : string; seq : int}
    | ICMPv4_Timestamp_reply of {id : string; seq : int}
    | ICMPv4_Information_request of {id : string; seq : int}
    | ICMPv4_Information_reply of {id : string; seq : int}

  (*$*)

  type icmpv4_header = ICMPv4 of {mutable ty : icmpv4_type}

  type icmpv4_payload_raw = string

  let icmpv4_header_to_icmpv4_type (ICMPv4 header) = header.ty

  let make_dummy_icmpv4_header () =
    ICMPv4 {ty = ICMPv4_Echo_reply {id = "\x01\x01"; seq = 0}}

  let update_icmpv4_header_ ty (ICMPv4 header) =
    (match ty with None -> () | Some x -> header.ty <- x);
    ICMPv4 header

  let icmpv4_payload_raw_to_byte_string = id

  let byte_string_to_icmpv4_payload_raw = id
end

module ICMPv6 = struct
  type icmpv6_type =
    | ICMPv6_Echo_reply
    | ICMPv6_Echo_request

  type icmpv6_header = ICMPv6 of {mutable ty : icmpv6_type}

  type icmpv6_payload_raw = string

  let icmpv6_header_to_icmpv6_type (ICMPv6 header) = header.ty

  let make_dummy_icmpv6_header () = ICMPv6 {ty = ICMPv6_Echo_reply}

  let update_icmpv6_header_ ty (ICMPv6 header) =
    (match ty with None -> () | Some x -> header.ty <- x);
    ICMPv6 header

  let icmpv6_payload_raw_to_byte_string = id

  let byte_string_to_icmpv6_payload_raw = id
end

module TCP = struct
  type tcp_port = int

  type tcp_header =
    | TCP of
        { mutable src_port : tcp_port
        ; mutable dst_port : tcp_port
        ; mutable ack : bool
        ; mutable rst : bool
        ; mutable syn : bool
        ; mutable fin : bool }

  type tcp_payload_raw = string

  let compare_tcp_port = compare

  let tcp_port_to_int = id

  let int_to_tcp_port = id

  let tcp_header_to_src_port (TCP header) = header.src_port

  let tcp_header_to_dst_port (TCP header) = header.dst_port

  let tcp_header_to_ack_flag (TCP header) = header.ack

  let tcp_header_to_rst_flag (TCP header) = header.rst

  let tcp_header_to_syn_flag (TCP header) = header.syn

  let tcp_header_to_fin_flag (TCP header) = header.fin

  let make_dummy_tcp_header () =
    TCP
      { src_port = 100
      ; dst_port = 1000
      ; ack = false
      ; rst = false
      ; syn = true
      ; fin = false }

  let update_tcp_header_ ~src_port ~dst_port ~ack ~rst ~syn ~fin (TCP header) =
    (match src_port with None -> () | Some x -> header.src_port <- x);
    (match dst_port with None -> () | Some x -> header.dst_port <- x);
    (match ack with None -> () | Some x -> header.ack <- x);
    (match rst with None -> () | Some x -> header.rst <- x);
    (match syn with None -> () | Some x -> header.syn <- x);
    (match fin with None -> () | Some x -> header.fin <- x);
    TCP header

  let tcp_payload_raw_to_byte_string = id

  let byte_string_to_tcp_payload_raw = id
end

module UDP = struct
  type udp_port = int

  type udp_header =
    | UDP of {mutable src_port : udp_port; mutable dst_port : udp_port}

  type udp_payload_raw = string

  let header_can_be_modified_inplace = false

  let compare_udp_port = compare

  let udp_port_to_int = id

  let int_to_udp_port = id

  let udp_header_to_src_port (UDP header) = header.src_port

  let udp_header_to_dst_port (UDP header) = header.dst_port

  let make_dummy_udp_header () = UDP {src_port = 100; dst_port = 1000}

  let update_udp_header_ ~src_port ~dst_port (UDP header) =
    (match src_port with None -> () | Some x -> header.src_port <- x);
    (match dst_port with None -> () | Some x -> header.dst_port <- x);
    UDP header

  let udp_payload_raw_to_byte_string = id

  let byte_string_to_udp_payload_raw = id
end
