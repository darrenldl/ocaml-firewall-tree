(*$ #use "src/tree_base.cinaps"
  $*)
module type Ether = sig
  type ether_addr

  type ether_header

  type ether_payload_raw

  val compare_ether_addr : ether_addr -> ether_addr -> int

  val ether_addr_to_byte_string : ether_addr -> string

  val byte_string_to_ether_addr : string -> ether_addr

  val ether_header_to_src_addr : ether_header -> ether_addr

  val ether_header_to_dst_addr : ether_header -> ether_addr

  val make_dummy_ether_header : unit -> ether_header

  val update_ether_header_ :
    src_addr:ether_addr option
    -> dst_addr:ether_addr option
    -> ether_header
    -> ether_header

  val update_ether_header_byte_string_ :
    src_addr:string option
    -> dst_addr:string option
    -> ether_header
    -> ether_header

  val ether_payload_raw_to_byte_string : ether_payload_raw -> string

  val byte_string_to_ether_payload_raw : string -> ether_payload_raw
end

module type IPv4 = sig
  type ipv4_addr

  type ipv4_header

  type ipv4_payload_raw

  val compare_ipv4_addr : ipv4_addr -> ipv4_addr -> int

  val ipv4_addr_to_byte_string : ipv4_addr -> string

  val byte_string_to_ipv4_addr : string -> ipv4_addr

  val ipv4_header_to_src_addr : ipv4_header -> ipv4_addr

  val ipv4_header_to_dst_addr : ipv4_header -> ipv4_addr

  val make_dummy_ipv4_header : unit -> ipv4_header

  val update_ipv4_header_ :
    src_addr:ipv4_addr option
    -> dst_addr:ipv4_addr option
    -> ipv4_header
    -> ipv4_header

  val update_ipv4_header_byte_string_ :
    src_addr:string option
    -> dst_addr:string option
    -> ipv4_header
    -> ipv4_header

  val ipv4_payload_raw_to_byte_string : ipv4_payload_raw -> string

  val byte_string_to_ipv4_payload_raw : string -> ipv4_payload_raw
end

module type IPv6 = sig
  type ipv6_addr

  type ipv6_header

  type ipv6_payload_raw

  val compare_ipv6_addr : ipv6_addr -> ipv6_addr -> int

  val ipv6_addr_to_byte_string : ipv6_addr -> string

  val byte_string_to_ipv6_addr : string -> ipv6_addr

  val ipv6_header_to_src_addr : ipv6_header -> ipv6_addr

  val ipv6_header_to_dst_addr : ipv6_header -> ipv6_addr

  val make_dummy_ipv6_header : unit -> ipv6_header

  val update_ipv6_header_ :
    src_addr:ipv6_addr option
    -> dst_addr:ipv6_addr option
    -> ipv6_header
    -> ipv6_header

  val update_ipv6_header_byte_string_ :
    src_addr:string option
    -> dst_addr:string option
    -> ipv6_header
    -> ipv6_header

  val ipv6_payload_raw_to_byte_string : ipv6_payload_raw -> string

  val byte_string_to_ipv6_payload_raw : string -> ipv6_payload_raw
end

module type ICMPv4 = sig
  type ipv4_addr

  (*$ print_icmpv4_type_def ()
  *)
  type icmpv4_type =
    | ICMPv4_Echo_reply of {id : string; seq : int}
    | ICMPv4_Destination_unreachable
    | ICMPv4_Source_quench
    | ICMPv4_Redirect
    | ICMPv4_Echo_request of {id : string; seq : int}
    | ICMPv4_Time_exceeded
    | ICMPv4_Parameter_problem
    | ICMPv4_Timestamp_request of {id : string; seq : int}
    | ICMPv4_Timestamp_reply of {id : string; seq : int}
    | ICMPv4_Information_request of {id : string; seq : int}
    | ICMPv4_Information_reply of {id : string; seq : int}

  (*$*)

  type icmpv4_header

  type icmpv4_payload_raw

  val icmpv4_header_to_icmpv4_type : icmpv4_header -> icmpv4_type

  val make_dummy_icmpv4_header : unit -> icmpv4_header

  val update_icmpv4_header_ :
    icmpv4_type option -> icmpv4_header -> icmpv4_header

  val icmpv4_payload_raw_to_byte_string : icmpv4_payload_raw -> string

  val byte_string_to_icmpv4_payload_raw : string -> icmpv4_payload_raw
end

module type ICMPv6 = sig
  type ipv6_addr

  type icmpv6_type =
    | ICMPv6_Echo_reply
    | ICMPv6_Echo_request

  type icmpv6_header

  type icmpv6_payload_raw

  val icmpv6_header_to_icmpv6_type : icmpv6_header -> icmpv6_type

  val make_dummy_icmpv6_header : unit -> icmpv6_header

  val update_icmpv6_header_ :
    icmpv6_type option -> icmpv6_header -> icmpv6_header

  val icmpv6_payload_raw_to_byte_string : icmpv6_payload_raw -> string

  val byte_string_to_icmpv6_payload_raw : string -> icmpv6_payload_raw
end

module type TCP = sig
  type tcp_header

  type tcp_payload_raw

  val tcp_header_to_src_port : tcp_header -> int

  val tcp_header_to_dst_port : tcp_header -> int

  val tcp_header_to_ack_flag : tcp_header -> bool

  val tcp_header_to_rst_flag : tcp_header -> bool

  val tcp_header_to_syn_flag : tcp_header -> bool

  val tcp_header_to_fin_flag : tcp_header -> bool

  val make_dummy_tcp_header : unit -> tcp_header

  val update_tcp_header_ :
    src_port:int option
    -> dst_port:int option
    -> ack:bool option
    -> rst:bool option
    -> syn:bool option
    -> fin:bool option
    -> tcp_header
    -> tcp_header

  val tcp_payload_raw_to_byte_string : tcp_payload_raw -> string

  val byte_string_to_tcp_payload_raw : string -> tcp_payload_raw
end

module type UDP = sig
  type udp_header

  type udp_payload_raw

  val udp_header_to_src_port : udp_header -> int

  val udp_header_to_dst_port : udp_header -> int

  val make_dummy_udp_header : unit -> udp_header

  val update_udp_header_ :
    src_port:int option -> dst_port:int option -> udp_header -> udp_header

  val udp_payload_raw_to_byte_string : udp_payload_raw -> string

  val byte_string_to_udp_payload_raw : string -> udp_payload_raw
end

module type B = sig
  type decision

  type netif

  val cur_time_ms : unit -> int64

  module Ether : Ether

  module IPv4 : IPv4

  module IPv6 : IPv6

  module ICMPv4 : ICMPv4 with type ipv4_addr := IPv4.ipv4_addr

  module ICMPv6 : ICMPv6 with type ipv6_addr := IPv6.ipv6_addr

  module TCP : TCP

  module UDP : UDP
end
