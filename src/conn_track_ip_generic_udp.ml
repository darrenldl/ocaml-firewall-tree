module type B = sig
  module Lookup_table : Lookup_table.S

  type conn_state =
    | New
    | Established
    | Invalid

  type 'a conn_state_w_key =
    | New of 'a
    | Established of 'a
    | Invalid

  val conn_state_to_conn_state_w_key : 'a -> conn_state -> 'a conn_state_w_key

  val cur_time_ms : unit -> int64

  type addr

  val addr_to_byte_string : addr -> string

  type udp_port
end

module type S = sig
  type addr

  type udp_port

  type key

  type conn_state

  type 'a conn_state_w_key

  type tracker

  val make : max_conn:int -> init_size:int -> timeout_ms:int64 -> tracker

  val lookup_conn_state_w_key :
    tracker
    -> reject_new:bool
    -> no_update:bool
    -> src_addr:addr
    -> src_port:udp_port
    -> dst_addr:addr
    -> dst_port:udp_port
    -> key conn_state_w_key
end

module Make (B : B) :
  S
  with type addr := B.addr
   and type udp_port := B.udp_port
   and type conn_state := B.conn_state
   and type 'a conn_state_w_key := 'a B.conn_state_w_key = struct
  include B

  type key =
    { initiator_addr : string
    ; initiator_port : udp_port
    ; responder_addr : string
    ; responder_port : udp_port }

  type record = {state : conn_state}

  type tracker = {table : (key, record) Lookup_table.t}

  let handle_possibly_established_connection table key record :
    key conn_state_w_key =
    let {state = stored_state; _} = record in
    match stored_state with
    | New ->
      Lookup_table.add table key {state = Established};
      Established key
    | Established ->
      Lookup_table.add table key {state = Established};
      Established key
    | _ ->
      Invalid

  let make ~(max_conn : int) ~(init_size : int) ~(timeout_ms : int64) : tracker
    =
    { table =
        Lookup_table.create ~timeout_ms:(Some timeout_ms) ~max_size:max_conn
          ~init_size }

  let lookup_conn_state_w_key (tracker : tracker) ~(reject_new : bool)
      ~(no_update : bool) ~(src_addr : addr) ~(src_port : udp_port)
      ~(dst_addr : addr) ~(dst_port : udp_port) : key conn_state_w_key =
    let {table} = tracker in
    let src_addr = addr_to_byte_string src_addr in
    let dst_addr = addr_to_byte_string dst_addr in
    (* check if cleanup is required *)
    Lookup_table.evict_timed_out table;
    (* check if the pair of addresses and ports are in the database in either direction *)
    let key_src_as_init =
      { initiator_addr = src_addr
      ; initiator_port = src_port
      ; responder_addr = dst_addr
      ; responder_port = dst_port }
    in
    let key_dst_as_init =
      { initiator_addr = dst_addr
      ; initiator_port = dst_port
      ; responder_addr = src_addr
      ; responder_port = src_port }
    in
    match
      ( Lookup_table.find_opt table key_src_as_init
      , Lookup_table.find_opt table key_dst_as_init )
    with
    | None, None ->
      (* possibly new *)
      if reject_new then Invalid
      else if no_update then New key_src_as_init
      else (
        Lookup_table.add table key_src_as_init {state = New};
        New key_src_as_init )
    | Some record, None ->
      if no_update then
        conn_state_to_conn_state_w_key key_src_as_init record.state
      else
        handle_possibly_established_connection table key_src_as_init record
    | None, Some record ->
      if no_update then
        conn_state_to_conn_state_w_key key_dst_as_init record.state
      else
        handle_possibly_established_connection table key_dst_as_init record
    | Some _, Some _ ->
      (* this state should not be reachable, cleanse everything *)
      Lookup_table.remove table key_src_as_init;
      Lookup_table.remove table key_dst_as_init;
      Invalid
end
