module type S = sig
  type pred

  type decision_tree_branch

  type conn_tracker

  val filter : pred -> decision_tree_branch -> decision_tree_branch

  val select_first_match :
    (pred * decision_tree_branch) array -> decision_tree_branch

  val load_balance_pdu_based_round_robin :
    decision_tree_branch array -> decision_tree_branch

  val load_balance_conn_based_round_robin :
    conn_tracker -> decision_tree_branch array -> decision_tree_branch
end

module Make (B : Tree.S) :
  S
  with type pred := B.Pred.pred
   and type decision_tree_branch := B.decision_tree_branch
   and type conn_tracker := B.Conn_track.conn_tracker = struct
  include B

  let filter (pred : Pred.pred) (next : decision_tree_branch) :
    decision_tree_branch =
    let logic_unit ~src_netif rlu_ipv4 rlu_ipv6 pdu _choices =
      if Pred.eval_pred pred ~src_netif rlu_ipv4 rlu_ipv6 pdu then 0 else -1
    in
    make_selector logic_unit [|next|]

  let select_first_match
      (pred_choices : (Pred.pred * decision_tree_branch) array) :
    decision_tree_branch =
    let choices = Array.map (fun (_, x) -> x) pred_choices in
    let preds = Array.map (fun (x, _) -> x) pred_choices in
    let logic_unit ~src_netif rlu_ipv4 rlu_ipv6 pdu _choices =
      let choice = ref None in
      Array.iteri
        (fun i pred ->
           match !choice with
           | Some _ ->
             ()
           | None ->
             if Pred.eval_pred pred ~src_netif rlu_ipv4 rlu_ipv6 pdu then
               choice := Some i )
        preds;
      match !choice with None -> -1 | Some i -> i
    in
    make_selector logic_unit choices

  let load_balance_pdu_based_round_robin (choices : decision_tree_branch array)
    : decision_tree_branch =
    let choice_count = Array.length choices in
    let counter = Misc_utils.make_wrap_around_counter_exc choice_count in
    let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 _pdu _choices =
      counter ()
    in
    make_selector logic_unit choices

  let load_balance_conn_based_round_robin (tracker : Conn_track.conn_tracker)
      (choices : decision_tree_branch array) : decision_tree_branch =
    let choice_count = Array.length choices in
    let max_size = 100 in
    let init_size = 50 in
    let timeout_ms = 30_000L in
    let table =
      Lookup_table.create ~timeout_ms:(Some timeout_ms) ~max_size ~init_size
    in
    let counter = Misc_utils.make_wrap_around_counter_exc choice_count in
    let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 pdu _choices =
      Lookup_table.evict_timed_out table;
      match Conn_track.lookup_conn_state_w_key tracker pdu with
      | New key ->
        let choice = counter () in
        Lookup_table.add table key choice;
        choice
      | Established key -> (
          match Lookup_table.find_opt table key with
          | None ->
            let choice = counter () in
            Lookup_table.add table key choice;
            choice
          | Some x ->
            x )
      | Invalid ->
        -1
    in
    make_selector logic_unit choices
end
