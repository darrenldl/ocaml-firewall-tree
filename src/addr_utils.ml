let string_and (a : string) (b : string) : string =
  assert (String.length a = String.length b);
  String.mapi
    (fun i char_in_a ->
       let char_in_b = b.[i] in
       Char.chr (Char.code char_in_a land Char.code char_in_b) )
    a

let mask_lookup = [|0x00; 0x80; 0xC0; 0xE0; 0xF0; 0xF8; 0xFC; 0xFE|]

let string_mask_first_n_bits (n : int) (s : string) : string =
  assert (n >= 0);
  String.mapi
    (fun i c ->
       let cur_window_start_bit_index = i * 8 in
       let cur_window_end_bit_index = ((i + 1) * 8) - 1 in
       let last_bit_index_inc = n - 1 in
       if cur_window_end_bit_index <= last_bit_index_inc then c
       else if
         cur_window_start_bit_index <= last_bit_index_inc
         && last_bit_index_inc <= cur_window_end_bit_index
       then
         let bits_to_mask =
           last_bit_index_inc - cur_window_start_bit_index + 1
         in
         let mask = mask_lookup.(bits_to_mask) in
         Char.chr (Char.code c land mask)
       else '\x00' )
    s
