(*$ #use "src/tree.cinaps"
    #use "src/pred.cinaps"
    #use "src/tree_base_extended.cinaps"
  $*)
module type S = sig
  include Tree_base_extended.S

  open IPv4
  open IPv6
  open PDU

  module RLU_IPv4 :
    RLU.S
    with type netif := netif
     and type addr := ipv4_addr
     and type subnet_mask := ipv4_addr subnet_mask

  module RLU_IPv6 :
    RLU.S
    with type netif := netif
     and type addr := ipv6_addr
     and type subnet_mask := ipv6_addr subnet_mask

  module Conn_track :
    Conn_track.S
    with type ipv4_addr := ipv4_addr
     and type ipv6_addr := ipv6_addr
     and type tcp_port := tcp_port
     and type udp_port := udp_port
     and type pdu := pdu
     and type icmpv4_type := ICMPv4.icmpv4_type

  module Pred :
    Pred.S
    with type netif := netif
     and type ether_addr := Ether.ether_addr
     and type ipv4_addr := IPv4.ipv4_addr
     and type ipv6_addr := IPv6.ipv6_addr
     and type tcp_port := tcp_port
     and type udp_port := udp_port
     and type rlu_ipv4 := RLU_IPv4.rlu
     and type rlu_ipv6 := RLU_IPv6.rlu
     and type conn_state := Conn_track.conn_state
     and type conn_tracker := Conn_track.conn_tracker
     and type pdu := PDU.pdu

  module Translator :
    Translator.S
    with type pdu := pdu
     and type ipv4_addr := ipv4_addr
     and type conn_tracker := Conn_track.conn_tracker

  type outcome = decision * pdu

  (*$ print_type_decision_tree_type_def ()
  *)
  type decision_tree =
    | Start of {default : decision; next : decision_tree_branch}

  and decision_tree_branch =
    | Select of selector
    | Modify of modifier
    | Select_and_modify of selecting_modifier
    | Scan of scanner
    | Change_default of {default : decision; next : decision_tree_branch}
    | End of decision

  and selector =
    | Selector of
        { logic_unit : selector_logic_unit
        ; choices : decision_tree_branch array }

  and modifier =
    | Modifier of
        { logic_unit : modifier_logic_unit
        ; next : decision_tree_branch }

  and selecting_modifier =
    | Selecting_modifier of
        { logic_unit : selecting_modifier_logic_unit
        ; choices : decision_tree_branch array }

  and scanner =
    | Scanner of {logic_unit : scanner_logic_unit; next : decision_tree_branch}

  and selector_logic_unit =
    src_netif:netif
    -> RLU_IPv4.rlu
    -> RLU_IPv6.rlu
    -> pdu
    -> decision_tree_branch array
    -> int

  and modifier_logic_unit =
    src_netif:netif -> RLU_IPv4.rlu -> RLU_IPv6.rlu -> pdu -> pdu option

  and selecting_modifier_logic_unit =
    src_netif:netif
    -> RLU_IPv4.rlu
    -> RLU_IPv6.rlu
    -> pdu
    -> decision_tree_branch array
    -> (int * pdu) option

  and scanner_logic_unit =
    src_netif:netif -> RLU_IPv4.rlu -> RLU_IPv6.rlu -> pdu -> unit

  (*$*)

  type check_config

  val make_selector :
    selector_logic_unit -> decision_tree_branch array -> decision_tree_branch

  val make_modifier :
    modifier_logic_unit -> decision_tree_branch -> decision_tree_branch

  val make_selecting_modifier :
    selecting_modifier_logic_unit
    -> decision_tree_branch array
    -> decision_tree_branch

  val make_scanner :
    scanner_logic_unit -> decision_tree_branch -> decision_tree_branch

  val decide :
    ?exn_handler:(exn -> unit)
    -> decision_tree
    -> src_netif:netif
    -> RLU_IPv4.rlu
    -> RLU_IPv6.rlu
    -> pdu
    -> outcome

  val make_check_config :
    ?count:int
    -> name:string
    -> (unit -> decision_tree)
    -> src_netif:netif
    -> (unit -> RLU_IPv4.rlu)
    -> (unit -> RLU_IPv6.rlu)
    -> (pdu -> outcome -> bool)
    -> check_config
end

module Make (B : Tree_base.B) :
  S
  (*$ print_tree_base_extended_type_constraints ()
  *)
  with type decision = B.decision
   and type netif = B.netif
   and type Ether.ether_addr = B.Ether.ether_addr
   and type Ether.ether_header = B.Ether.ether_header
   and type Ether.ether_payload_raw = B.Ether.ether_payload_raw
   and type IPv4.ipv4_addr = B.IPv4.ipv4_addr
   and type IPv4.ipv4_header = B.IPv4.ipv4_header
   and type IPv4.ipv4_payload_raw = B.IPv4.ipv4_payload_raw
   and type IPv6.ipv6_addr = B.IPv6.ipv6_addr
   and type IPv6.ipv6_header = B.IPv6.ipv6_header
   and type IPv6.ipv6_payload_raw = B.IPv6.ipv6_payload_raw
   and type ICMPv4.icmpv4_type = B.ICMPv4.icmpv4_type
   and type ICMPv4.icmpv4_header = B.ICMPv4.icmpv4_header
   and type ICMPv4.icmpv4_payload_raw = B.ICMPv4.icmpv4_payload_raw
   and type ICMPv6.icmpv6_type = B.ICMPv6.icmpv6_type
   and type ICMPv6.icmpv6_header = B.ICMPv6.icmpv6_header
   and type ICMPv6.icmpv6_payload_raw = B.ICMPv6.icmpv6_payload_raw
   and type TCP.tcp_header = B.TCP.tcp_header
   and type TCP.tcp_payload_raw = B.TCP.tcp_payload_raw
   and type UDP.udp_header = B.UDP.udp_header
   and type UDP.udp_payload_raw = B.UDP.udp_payload_raw (*$*) = struct
  module Tree_base_extended = Tree_base_extended.Make (B)

  module RLU_IPv4 = RLU.Make (struct
      type netif = B.netif

      type addr = B.IPv4.ipv4_addr

      type subnet_mask = B.IPv4.ipv4_addr Tree_base_extended.subnet_mask

      let addr_to_net_addr = Tree_base_extended.IPv4.ipv4_addr_to_net_addr
    end)

  module RLU_IPv6 = RLU.Make (struct
      type netif = B.netif

      type addr = B.IPv6.ipv6_addr

      type subnet_mask = B.IPv6.ipv6_addr Tree_base_extended.subnet_mask

      let addr_to_net_addr = Tree_base_extended.IPv6.ipv6_addr_to_net_addr
    end)

  module Conn_track = Conn_track.Make (Tree_base_extended)
  include Tree_base_extended
  open PDU

  module Pred = Pred.Make (struct
      include Tree_base_extended

      type rlu_ipv4 = RLU_IPv4.rlu

      type rlu_ipv6 = RLU_IPv6.rlu

      type conn_tracker = Conn_track.conn_tracker

      type conn_state = Conn_track.conn_state

      let lookup_conn_state = Conn_track.lookup_conn_state
    end)

  module Translator = Translator.Make (Tree_base_extended) (Conn_track)

  type outcome = decision * pdu

  (*$ print_type_decision_tree_type_def ()
  *)
  type decision_tree =
    | Start of {default : decision; next : decision_tree_branch}

  and decision_tree_branch =
    | Select of selector
    | Modify of modifier
    | Select_and_modify of selecting_modifier
    | Scan of scanner
    | Change_default of {default : decision; next : decision_tree_branch}
    | End of decision

  and selector =
    | Selector of
        { logic_unit : selector_logic_unit
        ; choices : decision_tree_branch array }

  and modifier =
    | Modifier of
        { logic_unit : modifier_logic_unit
        ; next : decision_tree_branch }

  and selecting_modifier =
    | Selecting_modifier of
        { logic_unit : selecting_modifier_logic_unit
        ; choices : decision_tree_branch array }

  and scanner =
    | Scanner of {logic_unit : scanner_logic_unit; next : decision_tree_branch}

  and selector_logic_unit =
    src_netif:netif
    -> RLU_IPv4.rlu
    -> RLU_IPv6.rlu
    -> pdu
    -> decision_tree_branch array
    -> int

  and modifier_logic_unit =
    src_netif:netif -> RLU_IPv4.rlu -> RLU_IPv6.rlu -> pdu -> pdu option

  and selecting_modifier_logic_unit =
    src_netif:netif
    -> RLU_IPv4.rlu
    -> RLU_IPv6.rlu
    -> pdu
    -> decision_tree_branch array
    -> (int * pdu) option

  and scanner_logic_unit =
    src_netif:netif -> RLU_IPv4.rlu -> RLU_IPv6.rlu -> pdu -> unit

  (*$*)

  let make_selector logic_unit choices =
    Select (Selector {logic_unit; choices})

  let make_modifier logic_unit next = Modify (Modifier {logic_unit; next})

  let make_selecting_modifier logic_unit choices =
    Select_and_modify (Selecting_modifier {logic_unit; choices})

  let make_scanner logic_unit next = Scan (Scanner {logic_unit; next})

  let default_exn_handler _exn = ()

  let decide ?(exn_handler : exn -> unit = default_exn_handler)
      (tree : decision_tree) ~(src_netif : netif) (rlu_ipv4 : RLU_IPv4.rlu)
      (rlu_ipv6 : RLU_IPv6.rlu) (pdu : pdu) : outcome =
    let rec aux (exn_handler : exn -> unit) (tree : decision_tree_branch)
        (default : decision) (src_netif : netif) (rlu_ipv4 : RLU_IPv4.rlu)
        (rlu_ipv6 : RLU_IPv6.rlu) (pdu : pdu) : outcome =
      match tree with
      | Select (Selector {logic_unit; choices}) -> (
          let choice =
            (* try *)
            let choice = logic_unit ~src_netif rlu_ipv4 rlu_ipv6 pdu choices in
            if choice < 0 || choice >= Array.length choices then None
            else Some choices.(choice)
            (* with _ as e -> exn_handler e; None *)
          in
          match choice with
          | Some next ->
            aux exn_handler next default src_netif rlu_ipv4 rlu_ipv6 pdu
          | None ->
            (default, pdu) )
      | Modify (Modifier {logic_unit; next}) -> (
          let modified =
            (* try logic_unit ~src_netif rlu_ipv4 rlu_ipv6 pdu with _ -> None *)
            logic_unit ~src_netif rlu_ipv4 rlu_ipv6 pdu
          in
          match modified with
          | Some pdu ->
            aux exn_handler next default src_netif rlu_ipv4 rlu_ipv6 pdu
          | None ->
            (default, pdu) )
      | Select_and_modify (Selecting_modifier {logic_unit; choices}) -> (
          let res =
            (* try *)
            match logic_unit ~src_netif rlu_ipv4 rlu_ipv6 pdu choices with
            | None ->
              None
            | Some (choice, pdu) ->
              if choice < 0 || choice >= Array.length choices then None
              else Some (choices.(choice), pdu)
              (* with _ -> None *)
          in
          match res with
          | Some (next, pdu) ->
            aux exn_handler next default src_netif rlu_ipv4 rlu_ipv6 pdu
          | None ->
            (default, pdu) )
      | Scan (Scanner {logic_unit; next}) ->
        (* try *)
        logic_unit ~src_netif rlu_ipv4 rlu_ipv6 pdu;
        aux exn_handler next default src_netif rlu_ipv4 rlu_ipv6 pdu
      (* with _ -> (default, pdu) *)
      | Change_default {default; next} ->
        aux exn_handler next default src_netif rlu_ipv4 rlu_ipv6 pdu
      | End d ->
        (d, pdu)
    in
    let (Start {default; next}) = tree in
    aux exn_handler next default src_netif rlu_ipv4 rlu_ipv6 pdu

  type check_config =
    { count : int
    ; name : string
    ; make_tree : unit -> decision_tree
    ; src_netif : netif
    ; make_rlu_ipv4 : unit -> RLU_IPv4.rlu
    ; make_rlu_ipv6 : unit -> RLU_IPv6.rlu
    ; pred : pdu -> outcome -> bool }

  let make_check_config ?(count : int = 10_000) ~(name : string)
      (make_tree : unit -> decision_tree) ~(src_netif : netif)
      (make_rlu_ipv4 : unit -> RLU_IPv4.rlu)
      (make_rlu_ipv6 : unit -> RLU_IPv6.rlu) (pred : pdu -> outcome -> bool) :
    check_config =
    {count; name; make_tree; src_netif; make_rlu_ipv4; make_rlu_ipv6; pred}
end
