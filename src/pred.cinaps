let print_type_pred_def ?(prefix_with_module_name : bool = false) () =
  let words =
    [ ("Ethernet", "Ether", "ether", "addr")
    ; ("IPv4", "IPv4", "ipv4", "addr")
    ; ("IPv6", "IPv6", "ipv6", "addr")
    ; ("TCP", "TCP", "tcp", "port")
    ; ("UDP", "UDP", "udp", "port") ]
  in
  let ops = ["eq"; "gt"; "lt"; "ge"; "le"] in
  let src_dst = ["src"; "dst"] in
  print_endline
    {|
       type pred =
         | True
         | False
         | And of pred * pred
         | Or of pred * pred
         | Not of pred
    |};
  for i = 3 to 4 do
    Printf.printf
      {|
              | Guide_decap_to_layer%d of pred
            |} i
  done;
  List.iter
    (fun (comment, pdu_con, proto, field) ->
       Printf.printf {|
           (* %s *)
           | Contains_%s
         |}
         comment pdu_con;
       List.iter
         (fun src_dst ->
            Printf.printf "(* %s - %s *)" comment src_dst;
            Printf.printf
              {|
                 | %s_%s_%s_one_of of %s_%s array
            |}
              pdu_con src_dst field proto field;
            List.iter
              (fun op ->
                 Printf.printf
                   {|
                          | %s_%s_%s_%s of %s_%s
                        |}
                   pdu_con src_dst field op proto field )
              ops )
         src_dst )
    words;
  print_endline
    {|
      (* ICMPv4 *)
      | Contains_ICMPv4
      | ICMPv4_ty_eq_Echo_reply
      | ICMPv4_ty_eq_Destination_unreachable
      | ICMPv4_ty_eq_Source_quench
      | ICMPv4_ty_eq_Redirect
      | ICMPv4_ty_eq_Echo_request
      | ICMPv4_ty_eq_Time_exceeded
      | ICMPv4_ty_eq_Parameter_problem
      | ICMPv4_ty_eq_Timestamp_request
      | ICMPv4_ty_eq_Timestamp_reply
      | ICMPv4_ty_eq_Information_request
      | ICMPv4_ty_eq_Information_reply
    |};
  if prefix_with_module_name then
    print_endline
      {|
       (* connection tracking *)
       | Conn_state_eq of {tracker : Conn_track.conn_tracker; target_state : Conn_track.conn_state}
       (* custom *)
       | Custom of (netif -> RLU_IPv4.rlu -> RLU_IPv6.rlu -> pdu -> bool)
    |}
  else
    print_endline
      {|
       (* connection tracking *)
       | Conn_state_eq of {tracker : conn_tracker; target_state : conn_state}
       (* custom *)
       | Custom of (netif -> rlu_ipv4 -> rlu_ipv6 -> pdu -> bool)
    |}
