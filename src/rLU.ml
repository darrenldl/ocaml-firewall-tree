module type B = sig
  type netif

  type addr

  type subnet_mask

  val addr_to_net_addr : addr:addr -> mask:subnet_mask -> addr
end

module type S = sig
  type netif

  type addr

  type subnet_mask

  type rlu

  type outcome =
    { out_netif : netif
    ; next_hop_addr : addr }

  type route =
    | Local of {dst_addr : addr; subnet_mask : subnet_mask; netif : netif}
    | Remote of
        { dst_addr : addr
        ; subnet_mask : subnet_mask
        ; netif : netif
        ; next_hop_addr : addr }

  val make : unit -> rlu

  val route : rlu -> dst_addr:addr -> outcome option

  val add_route : rlu -> route -> unit

  val add_route_local :
    rlu -> dst_addr:addr -> subnet_mask:subnet_mask -> netif:netif -> unit

  val add_route_remote :
    rlu
    -> dst_addr:addr
    -> subnet_mask:subnet_mask
    -> netif:netif
    -> next_hop_addr:addr
    -> unit

  val add_routes : rlu -> route list -> unit

  val remove_route : rlu -> route -> unit

  val remove_routes : rlu -> route list -> unit
end

module Make (B : B) :
  S
  with type netif := B.netif
   and type addr := B.addr
   and type subnet_mask := B.subnet_mask = struct
  include B

  type outcome =
    { out_netif : netif
    ; next_hop_addr : addr }

  type route =
    | Local of {dst_addr : addr; subnet_mask : subnet_mask; netif : netif}
    | Remote of
        { dst_addr : addr
        ; subnet_mask : subnet_mask
        ; netif : netif
        ; next_hop_addr : addr }

  type rlu = {routes : route Linked_queue.t}

  let make () : rlu = {routes = Linked_queue.create ()}

  let add_route (rlu : rlu) (route : route) : unit =
    Linked_queue.push route rlu.routes

  let add_route_local (rlu : rlu) ~(dst_addr : addr)
      ~(subnet_mask : subnet_mask) ~(netif : netif) : unit =
    let route = Local {dst_addr; subnet_mask; netif} in
    add_route rlu route

  let add_route_remote (rlu : rlu) ~(dst_addr : addr)
      ~(subnet_mask : subnet_mask) ~(netif : netif) ~(next_hop_addr : addr) :
    unit =
    let route = Remote {dst_addr; subnet_mask; netif; next_hop_addr} in
    add_route rlu route

  let add_routes (rlu : rlu) (routes : route list) : unit =
    Linked_queue.push_all routes rlu.routes

  let remove_route (rlu : rlu) (route : route) : unit =
    Linked_queue.filter_inplace (fun r -> r <> route) rlu.routes

  let remove_routes (rlu : rlu) (routes : route list) : unit =
    Linked_queue.filter_inplace (fun r -> not (List.mem r routes)) rlu.routes

  let route (rlu : rlu) ~(dst_addr : addr) : outcome option =
    let res = ref None in
    Linked_queue.iter
      (fun route ->
         match !res with
         | Some _ ->
           ()
         | None -> (
             match route with
             | Local {dst_addr; subnet_mask; netif} ->
               let dst_net' =
                 B.addr_to_net_addr ~addr:dst_addr ~mask:subnet_mask
               in
               if dst_net' = dst_addr then
                 res := Some {out_netif = netif; next_hop_addr = dst_addr}
             | Remote {dst_addr; subnet_mask; netif; next_hop_addr} ->
               let dst_net' =
                 B.addr_to_net_addr ~addr:dst_addr ~mask:subnet_mask
               in
               if dst_net' = dst_addr then
                 res := Some {out_netif = netif; next_hop_addr} ) )
      rlu.routes;
    !res
end
