module type S = sig
  type ('a, 'b) t

  val create :
    max_size:int -> init_size:int -> timeout_ms:int64 option -> ('a, 'b) t

  val add : ('a, 'b) t -> 'a -> 'b -> unit

  val remove : ('a, 'b) t -> 'a -> unit

  val evict_timed_out : ('a, 'b) t -> unit

  val find_opt : ('a, 'b) t -> 'a -> 'b option

  val find_opt2 : ('a, 'b) t -> ('b, 'c) t -> 'a -> ('b * 'c) option

  val find_opt3 :
    ('a, 'b) t -> ('b, 'c) t -> ('c, 'd) t -> 'a -> ('b * 'c * 'd) option

  val find : ('a, 'b) t -> 'a -> 'b

  val update_timestamp : ('a, 'b) t -> 'a -> unit

  val of_alist :
    timeout_ms:int64 option -> max_size:int -> ('a * 'b) list -> ('a, 'b) t

  val add_bidirectional_mapping : ('a, 'b) t -> ('b, 'a) t -> 'a -> 'b -> unit
end

module Make (B : Tree_base.B) : S = struct
  include B

  type 'a record =
    { timestamp : int64
    ; value : 'a }

  type ('a, 'b) t =
    { table : ('a, 'b record) Hashtbl.t
    ; queue : 'a Linked_queue.t
    ; max_size : int
    ; mutable last_cleanup : int64
    ; timeout_ms : int64 option }

  let create ~(max_size : int) ~(init_size : int) ~(timeout_ms : int64 option)
    : ('a, 'b) t =
    { table = Hashtbl.create ~random:true init_size
    ; queue = Linked_queue.create ()
    ; max_size
    ; last_cleanup = cur_time_ms ()
    ; timeout_ms }

  let remove_internal ?(skip_queue : bool = false) (t : ('a, 'b) t) (key : 'a)
    : unit =
    Hashtbl.filter_map_inplace
      (fun k v -> if k = key then Some v else None)
      t.table;
    if not skip_queue then
      Linked_queue.filter_inplace (fun k -> k <> key) t.queue

  let remove (t : ('a, 'b) t) (key : 'a) : unit = remove_internal t key

  let add (t : ('a, 'b) t) (key : 'a) (value : 'b) : unit =
    (* clear space if necessary *)
    let count = Hashtbl.length t.table in
    ( if count = t.max_size then
        (* evict oldest added connection *)
        match Linked_queue.pop_opt t.queue with
        | None ->
          ()
        | Some oldest_key ->
          remove_internal ~skip_queue:true t oldest_key );
    (* add key and record *)
    let timestamp = cur_time_ms () in
    Hashtbl.replace t.table key {timestamp; value};
    Linked_queue.push key t.queue

  let add_bidirectional_mapping (t1 : ('a, 'b) t) (t2 : ('b, 'a) t) (key : 'a)
      (value : 'b) : unit =
    add t1 key value; add t2 value key

  let evict_timed_out (t : ('a, 'b) t) : unit =
    match t.timeout_ms with
    | None ->
      ()
    | Some timeout_ms ->
      let cur_time = cur_time_ms () in
      if Int64.compare (Int64.sub cur_time t.last_cleanup) timeout_ms >= 0
      then (
        let timed_out_keys = Hashset.create 50 in
        Hashtbl.filter_map_inplace
          (fun k {timestamp; value} ->
             let time_diff = Int64.sub cur_time timestamp in
             if Int64.compare time_diff timeout_ms >= 0 then (
               Hashset.add timed_out_keys k;
               None )
             else Some {timestamp; value} )
          t.table;
        Linked_queue.filter_inplace
          (fun k -> Hashset.mem timed_out_keys k)
          t.queue;
        t.last_cleanup <- cur_time )

  let replace_internal (t : ('a, 'b) t) (k : 'a) (value : 'b) : unit =
    let timestamp = cur_time_ms () in
    Hashtbl.replace t.table k {timestamp; value}

  let find_opt (t : ('a, 'b) t) (k : 'a) : 'b option =
    match Hashtbl.find_opt t.table k with
    | Some {value; _} ->
      replace_internal t k value; Some value
    | None ->
      None

  let find_opt2 (t1 : ('a, 'b) t) (t2 : ('b, 'c) t) (k : 'a) : ('b * 'c) option
    =
    match find_opt t1 k with
    | None ->
      None
    | Some x -> (
        match find_opt t2 x with None -> None | Some y -> Some (x, y) )

  let find_opt3 (t1 : ('a, 'b) t) (t2 : ('b, 'c) t) (t3 : ('c, 'd) t) (k : 'a)
    : ('b * 'c * 'd) option =
    match find_opt t1 k with
    | None ->
      None
    | Some x -> (
        match find_opt t2 x with
        | None ->
          None
        | Some y -> (
            match find_opt t3 y with None -> None | Some z -> Some (x, y, z) ) )

  let find (t : ('a, 'b) t) (k : 'a) : 'b =
    let {value; _} = Hashtbl.find t.table k in
    replace_internal t k value; value

  let update_timestamp (t : ('a, 'b) t) (k : 'a) : unit =
    match find_opt t k with None -> () | Some v -> replace_internal t k v

  let of_alist ~(timeout_ms : int64 option) ~(max_size : int)
      (l : ('a * 'b) list) : ('a, 'b) t =
    let init_size = List.length l in
    let t = create ~timeout_ms ~max_size ~init_size in
    List.iter (fun (k, v) -> add t k v) l;
    t
end
