type 'a t = ('a, unit) Hashtbl.t

let create (size : int) : 'a t = Hashtbl.create ~random:true size

let remove (t : 'a t) (v : 'a) : unit = Hashtbl.remove t v

let add (t : 'a t) (v : 'a) : unit = remove t v; Hashtbl.add t v ()

let mem (t : 'a t) (v : 'a) : bool = Hashtbl.mem t v
