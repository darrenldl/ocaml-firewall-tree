(*$ #use "src/tree_base.cinaps"
  $*)
module type S = sig
  type ipv4_addr

  type ipv6_addr

  type tcp_port

  type udp_port

  type icmpv4_type

  type pdu

  type key

  type conn_state =
    | New
    | Established
    | Invalid

  type 'a conn_state_w_key =
    | New of 'a
    | Established of 'a
    | Invalid

  type tcp_flag =
    | Syn
    | Ack
    | Syn_and_ack
    | Fin_and_ack
    | No_flags

  type conn_tracker

  val make : max_conn:int -> init_size:int -> timeout_ms:int64 -> conn_tracker

  val lookup_conn_state :
    ?reject_new:bool -> ?no_update:bool -> conn_tracker -> pdu -> conn_state

  val lookup_conn_state_w_key :
    ?reject_new:bool
    -> ?no_update:bool
    -> conn_tracker
    -> pdu
    -> key conn_state_w_key

  val pass_pdu_through_tracker : conn_tracker -> pdu -> unit

  module IPv4_TCP_conn_track : sig
    type tracker

    type key

    val lookup_conn_state_w_key :
      tracker
      -> reject_new:bool
      -> no_update:bool
      -> src_addr:ipv4_addr
      -> src_port:tcp_port
      -> dst_addr:ipv4_addr
      -> dst_port:tcp_port
      -> tcp_flag
      -> key conn_state_w_key
  end

  module IPv6_TCP_conn_track : sig
    type tracker

    type key

    val lookup_conn_state_w_key :
      tracker
      -> reject_new:bool
      -> no_update:bool
      -> src_addr:ipv6_addr
      -> src_port:tcp_port
      -> dst_addr:ipv6_addr
      -> dst_port:tcp_port
      -> tcp_flag
      -> key conn_state_w_key
  end

  module IPv4_UDP_conn_track : sig
    type tracker

    type key

    val lookup_conn_state_w_key :
      tracker
      -> reject_new:bool
      -> no_update:bool
      -> src_addr:ipv4_addr
      -> src_port:udp_port
      -> dst_addr:ipv4_addr
      -> dst_port:udp_port
      -> key conn_state_w_key
  end

  module IPv6_UDP_conn_track : sig
    type tracker

    type key

    val lookup_conn_state_w_key :
      tracker
      -> reject_new:bool
      -> no_update:bool
      -> src_addr:ipv6_addr
      -> src_port:udp_port
      -> dst_addr:ipv6_addr
      -> dst_port:udp_port
      -> key conn_state_w_key
  end

  module ICMPv4_conn_track : sig
    type tracker

    type key

    val lookup_conn_state_w_key :
      tracker
      -> reject_new:bool
      -> no_update:bool
      -> src_addr:ipv4_addr
      -> dst_addr:ipv4_addr
      -> icmpv4_type
      -> key conn_state_w_key
  end
end

module Make (B : Tree_base_extended.S) :
  S
  with type ipv4_addr := B.IPv4.ipv4_addr
   and type ipv6_addr := B.IPv6.ipv6_addr
   and type tcp_port := B.tcp_port
   and type udp_port := B.udp_port
   and type icmpv4_type := B.ICMPv4.icmpv4_type
   and type pdu := B.PDU.pdu = struct
  include B
  open PDU

  module Shared = struct
    type conn_state =
      | New
      | Established
      | Invalid

    type 'a conn_state_w_key =
      | New of 'a
      | Established of 'a
      | Invalid

    let conn_state_to_conn_state_w_key (v : 'a) (x : conn_state) :
      'a conn_state_w_key =
      match x with
      | New ->
        New v
      | Established ->
        Established v
      | Invalid ->
        Invalid

    let conn_state_w_key_to_conn_state (x : 'a conn_state_w_key) : conn_state =
      match x with
      | New _ ->
        New
      | Established _ ->
        Established
      | Invalid ->
        Invalid

    let conn_state_w_key_map (f : 'a -> 'b) (x : 'a conn_state_w_key) :
      'b conn_state_w_key =
      match x with
      | New x ->
        New (f x)
      | Established x ->
        Established (f x)
      | Invalid ->
        Invalid

    type tcp_flag =
      | Syn
      | Ack
      | Syn_and_ack
      | Fin_and_ack
      | No_flags
  end

  include Shared

  module IPv4_TCP_conn_track = Conn_track_ip_generic_tcp.Make (struct
      include Shared
      module Lookup_table = Lookup_table

      type addr = B.IPv4.ipv4_addr

      let addr_to_byte_string = B.IPv4.ipv4_addr_to_byte_string

      type tcp_port = B.tcp_port

      let cur_time_ms = B.cur_time_ms
    end)

  module IPv6_TCP_conn_track = Conn_track_ip_generic_tcp.Make (struct
      include Shared
      module Lookup_table = Lookup_table

      type addr = B.IPv6.ipv6_addr

      let addr_to_byte_string = B.IPv6.ipv6_addr_to_byte_string

      type tcp_port = B.tcp_port

      let cur_time_ms = B.cur_time_ms
    end)

  module IPv4_UDP_conn_track = Conn_track_ip_generic_udp.Make (struct
      include Shared
      module Lookup_table = Lookup_table

      type addr = B.IPv4.ipv4_addr

      let addr_to_byte_string = B.IPv4.ipv4_addr_to_byte_string

      type udp_port = B.udp_port

      let cur_time_ms = B.cur_time_ms
    end)

  module IPv6_UDP_conn_track = Conn_track_ip_generic_udp.Make (struct
      include Shared
      module Lookup_table = Lookup_table

      type addr = B.IPv6.ipv6_addr

      let addr_to_byte_string = B.IPv6.ipv6_addr_to_byte_string

      type udp_port = B.udp_port

      let cur_time_ms = B.cur_time_ms
    end)

  module ICMPv4_conn_track = Conn_track_icmpv4.Make (struct
      include Shared
      include B.ICMPv4
      module Lookup_table = Lookup_table

      type addr = B.IPv4.ipv4_addr

      let addr_to_byte_string = B.IPv4.ipv4_addr_to_byte_string

      let cur_time_ms = B.cur_time_ms
    end)

  type key =
    | IPv4_TCP_key of IPv4_TCP_conn_track.key
    | IPv4_UDP_key of IPv4_UDP_conn_track.key
    | IPv6_UDP_key of IPv6_UDP_conn_track.key
    | IPv6_TCP_key of IPv6_TCP_conn_track.key
    | ICMPv4_key of ICMPv4_conn_track.key

  type conn_tracker =
    { ipv4_tcp_tracker : IPv4_TCP_conn_track.tracker
    ; ipv4_udp_tracker : IPv4_UDP_conn_track.tracker
    ; ipv6_tcp_tracker : IPv6_TCP_conn_track.tracker
    ; ipv6_udp_tracker : IPv6_UDP_conn_track.tracker
    ; icmpv4_tracker : ICMPv4_conn_track.tracker }

  let make ~(max_conn : int) ~(init_size : int) ~(timeout_ms : int64) :
    conn_tracker =
    { ipv4_tcp_tracker =
        IPv4_TCP_conn_track.make ~max_conn ~init_size ~timeout_ms
    ; ipv4_udp_tracker =
        IPv4_UDP_conn_track.make ~max_conn ~init_size ~timeout_ms
    ; ipv6_tcp_tracker =
        IPv6_TCP_conn_track.make ~max_conn ~init_size ~timeout_ms
    ; ipv6_udp_tracker =
        IPv6_UDP_conn_track.make ~max_conn ~init_size ~timeout_ms
    ; icmpv4_tracker = ICMPv4_conn_track.make ~max_conn ~init_size ~timeout_ms
    }

  let tcp_header_to_conntrack_tcp_flag tcp_header =
    let ack = TCP.tcp_header_to_ack_flag tcp_header in
    let syn = TCP.tcp_header_to_syn_flag tcp_header in
    let fin = TCP.tcp_header_to_fin_flag tcp_header in
    if ack && syn then Syn_and_ack
    else if ack && fin then Fin_and_ack
    else if syn then Syn
    else if ack then Ack
    else No_flags

  let lookup_conn_state_w_key ?(reject_new : bool = false)
      ?(no_update : bool = false) (tracker : conn_tracker) (pdu : pdu) :
    key conn_state_w_key =
    match PDU_to.layer3_pdu pdu with
    (* TCP connection tracking *)
    | Some
        (IPv4
           (IPv4_pkt
              { header = ipv4_header
              ; payload =
                  IPv4_payload_encap
                    (TCP (TCP_pdu {header = tcp_header; payload = _})) })) ->
      let src_addr = IPv4.ipv4_header_to_src_addr ipv4_header in
      let src_port = TCP.tcp_header_to_src_port tcp_header in
      let dst_addr = IPv4.ipv4_header_to_dst_addr ipv4_header in
      let dst_port = TCP.tcp_header_to_dst_port tcp_header in
      let flag = tcp_header_to_conntrack_tcp_flag tcp_header in
      conn_state_w_key_map
        (fun x -> IPv4_TCP_key x)
        (IPv4_TCP_conn_track.lookup_conn_state_w_key tracker.ipv4_tcp_tracker
           ~reject_new ~no_update ~src_addr ~src_port ~dst_addr ~dst_port
           flag)
    | Some
        (IPv6
           (IPv6_pkt
              { header = ipv6_header
              ; payload =
                  IPv6_payload_encap
                    (TCP (TCP_pdu {header = tcp_header; payload = _})) })) ->
      let src_addr = IPv6.ipv6_header_to_src_addr ipv6_header in
      let src_port = TCP.tcp_header_to_src_port tcp_header in
      let dst_addr = IPv6.ipv6_header_to_dst_addr ipv6_header in
      let dst_port = TCP.tcp_header_to_dst_port tcp_header in
      let flag = tcp_header_to_conntrack_tcp_flag tcp_header in
      conn_state_w_key_map
        (fun x -> IPv6_TCP_key x)
        (IPv6_TCP_conn_track.lookup_conn_state_w_key tracker.ipv6_tcp_tracker
           ~reject_new ~no_update ~src_addr ~src_port ~dst_addr ~dst_port
           flag)
    (* UDP connection tracking *)
    | Some
        (IPv4
           (IPv4_pkt
              { header = ipv4_header
              ; payload =
                  IPv4_payload_encap
                    (UDP (UDP_pdu {header = udp_header; payload = _})) })) ->
      let src_addr = IPv4.ipv4_header_to_src_addr ipv4_header in
      let src_port = UDP.udp_header_to_src_port udp_header in
      let dst_addr = IPv4.ipv4_header_to_dst_addr ipv4_header in
      let dst_port = UDP.udp_header_to_dst_port udp_header in
      conn_state_w_key_map
        (fun x -> IPv4_UDP_key x)
        (IPv4_UDP_conn_track.lookup_conn_state_w_key tracker.ipv4_udp_tracker
           ~reject_new ~no_update ~src_addr ~src_port ~dst_addr ~dst_port)
    | Some
        (IPv6
           (IPv6_pkt
              { header = ipv6_header
              ; payload =
                  IPv6_payload_encap
                    (UDP (UDP_pdu {header = udp_header; payload = _})) })) ->
      let src_addr = IPv6.ipv6_header_to_src_addr ipv6_header in
      let src_port = UDP.udp_header_to_src_port udp_header in
      let dst_addr = IPv6.ipv6_header_to_dst_addr ipv6_header in
      let dst_port = UDP.udp_header_to_dst_port udp_header in
      conn_state_w_key_map
        (fun x -> IPv6_UDP_key x)
        (IPv6_UDP_conn_track.lookup_conn_state_w_key tracker.ipv6_udp_tracker
           ~reject_new ~no_update ~src_addr ~src_port ~dst_addr ~dst_port)
    (* ICMP connection tracking *)
    | Some
        (IPv4
           (IPv4_pkt
              { header = ipv4_header
              ; payload =
                  IPv4_payload_icmp
                    (ICMPv4_pkt
                       {header = icmpv4_header; payload = ICMPv4_payload_raw _})
              })) ->
      let src_addr = IPv4.ipv4_header_to_src_addr ipv4_header in
      let dst_addr = IPv4.ipv4_header_to_dst_addr ipv4_header in
      let ty = ICMPv4.icmpv4_header_to_icmpv4_type icmpv4_header in
      conn_state_w_key_map
        (fun x -> ICMPv4_key x)
        (ICMPv4_conn_track.lookup_conn_state_w_key tracker.icmpv4_tracker
           ~reject_new ~no_update ~src_addr ~dst_addr ty)
    | _ ->
      Invalid

  let lookup_conn_state ?(reject_new : bool = false)
      ?(no_update : bool = false) (tracker : conn_tracker) (pdu : pdu) :
    conn_state =
    conn_state_w_key_to_conn_state
      (lookup_conn_state_w_key ~reject_new ~no_update tracker pdu)

  let pass_pdu_through_tracker (tracker : conn_tracker) (pdu : pdu) : unit =
    lookup_conn_state tracker pdu |> ignore
end
