module type S = sig
  type decision_tree_branch

  type conn_tracker

  val pass_pdu_through_conn_tracker :
    conn_tracker -> decision_tree_branch -> decision_tree_branch
end

module Make (B : Tree.S) :
  S
  with type decision_tree_branch := B.decision_tree_branch
   and type conn_tracker := B.Conn_track.conn_tracker = struct
  include B

  let pass_pdu_through_conn_tracker (tracker : Conn_track.conn_tracker)
      (next : decision_tree_branch) : decision_tree_branch =
    let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 pdu =
      Conn_track.pass_pdu_through_tracker tracker pdu
    in
    Scan (Scanner {logic_unit; next})
end
