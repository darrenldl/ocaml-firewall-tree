module type B = Tree_base.B

module type S = Tree.S

module Make = Tree.Make
module Selectors = Selectors
module Modifiers = Modifiers
module Scanners = Scanners
module Addr_utils = Addr_utils

module Mock_tree_base : B
