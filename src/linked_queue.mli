type 'a t

val create : unit -> 'a t

val filter_inplace : ('a -> bool) -> 'a t -> unit

val push : 'a -> 'a t -> unit

val push_all : 'a list -> 'a t -> unit

val pop : 'a t -> 'a

val pop_opt : 'a t -> 'a option

val to_list : 'a t -> 'a list

val iter : ('a -> unit) -> 'a t -> unit
