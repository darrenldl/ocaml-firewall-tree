module type B = sig
  include Tree_base_extended.S

  open PDU

  type rlu_ipv4

  type rlu_ipv6

  type conn_state

  type conn_tracker

  val lookup_conn_state :
    ?reject_new:bool -> ?no_update:bool -> conn_tracker -> pdu -> conn_state
end

(*$ #use "src/pred.cinaps"
  $*)

module type S = sig
  type netif

  type ether_addr

  type ipv4_addr

  type ipv6_addr

  type tcp_port

  type udp_port

  type rlu_ipv4

  type rlu_ipv6

  type conn_state

  type conn_tracker

  type pdu

  (*$ print_type_pred_def ()
  *)
  type pred =
    | True
    | False
    | And of pred * pred
    | Or of pred * pred
    | Not of pred
    | Guide_decap_to_layer3 of pred
    | Guide_decap_to_layer4 of pred
    (* Ethernet *)
    | Contains_Ether
    (* Ethernet - src *)
    | Ether_src_addr_one_of of ether_addr array
    | Ether_src_addr_eq of ether_addr
    | Ether_src_addr_gt of ether_addr
    | Ether_src_addr_lt of ether_addr
    | Ether_src_addr_ge of ether_addr
    | Ether_src_addr_le of ether_addr
    (* Ethernet - dst *)
    | Ether_dst_addr_one_of of ether_addr array
    | Ether_dst_addr_eq of ether_addr
    | Ether_dst_addr_gt of ether_addr
    | Ether_dst_addr_lt of ether_addr
    | Ether_dst_addr_ge of ether_addr
    | Ether_dst_addr_le of ether_addr
    (* IPv4 *)
    | Contains_IPv4
    (* IPv4 - src *)
    | IPv4_src_addr_one_of of ipv4_addr array
    | IPv4_src_addr_eq of ipv4_addr
    | IPv4_src_addr_gt of ipv4_addr
    | IPv4_src_addr_lt of ipv4_addr
    | IPv4_src_addr_ge of ipv4_addr
    | IPv4_src_addr_le of ipv4_addr
    (* IPv4 - dst *)
    | IPv4_dst_addr_one_of of ipv4_addr array
    | IPv4_dst_addr_eq of ipv4_addr
    | IPv4_dst_addr_gt of ipv4_addr
    | IPv4_dst_addr_lt of ipv4_addr
    | IPv4_dst_addr_ge of ipv4_addr
    | IPv4_dst_addr_le of ipv4_addr
    (* IPv6 *)
    | Contains_IPv6
    (* IPv6 - src *)
    | IPv6_src_addr_one_of of ipv6_addr array
    | IPv6_src_addr_eq of ipv6_addr
    | IPv6_src_addr_gt of ipv6_addr
    | IPv6_src_addr_lt of ipv6_addr
    | IPv6_src_addr_ge of ipv6_addr
    | IPv6_src_addr_le of ipv6_addr
    (* IPv6 - dst *)
    | IPv6_dst_addr_one_of of ipv6_addr array
    | IPv6_dst_addr_eq of ipv6_addr
    | IPv6_dst_addr_gt of ipv6_addr
    | IPv6_dst_addr_lt of ipv6_addr
    | IPv6_dst_addr_ge of ipv6_addr
    | IPv6_dst_addr_le of ipv6_addr
    (* TCP *)
    | Contains_TCP
    (* TCP - src *)
    | TCP_src_port_one_of of tcp_port array
    | TCP_src_port_eq of tcp_port
    | TCP_src_port_gt of tcp_port
    | TCP_src_port_lt of tcp_port
    | TCP_src_port_ge of tcp_port
    | TCP_src_port_le of tcp_port
    (* TCP - dst *)
    | TCP_dst_port_one_of of tcp_port array
    | TCP_dst_port_eq of tcp_port
    | TCP_dst_port_gt of tcp_port
    | TCP_dst_port_lt of tcp_port
    | TCP_dst_port_ge of tcp_port
    | TCP_dst_port_le of tcp_port
    (* UDP *)
    | Contains_UDP
    (* UDP - src *)
    | UDP_src_port_one_of of udp_port array
    | UDP_src_port_eq of udp_port
    | UDP_src_port_gt of udp_port
    | UDP_src_port_lt of udp_port
    | UDP_src_port_ge of udp_port
    | UDP_src_port_le of udp_port
    (* UDP - dst *)
    | UDP_dst_port_one_of of udp_port array
    | UDP_dst_port_eq of udp_port
    | UDP_dst_port_gt of udp_port
    | UDP_dst_port_lt of udp_port
    | UDP_dst_port_ge of udp_port
    | UDP_dst_port_le of udp_port
    (* ICMPv4 *)
    | Contains_ICMPv4
    | ICMPv4_ty_eq_Echo_reply
    | ICMPv4_ty_eq_Destination_unreachable
    | ICMPv4_ty_eq_Source_quench
    | ICMPv4_ty_eq_Redirect
    | ICMPv4_ty_eq_Echo_request
    | ICMPv4_ty_eq_Time_exceeded
    | ICMPv4_ty_eq_Parameter_problem
    | ICMPv4_ty_eq_Timestamp_request
    | ICMPv4_ty_eq_Timestamp_reply
    | ICMPv4_ty_eq_Information_request
    | ICMPv4_ty_eq_Information_reply
    (* connection tracking *)
    | Conn_state_eq of {tracker : conn_tracker; target_state : conn_state}
    (* custom *)
    | Custom of (netif -> rlu_ipv4 -> rlu_ipv6 -> pdu -> bool)

  (*$*)

  val eval_pred :
    pred -> src_netif:netif -> rlu_ipv4 -> rlu_ipv6 -> pdu -> bool

  val ether_addr_eq : ether_addr -> ether_addr -> bool

  val ether_addr_gt : ether_addr -> ether_addr -> bool

  val ether_addr_lt : ether_addr -> ether_addr -> bool

  val ether_addr_ge : ether_addr -> ether_addr -> bool

  val ether_addr_le : ether_addr -> ether_addr -> bool

  val ipv4_addr_eq : ipv4_addr -> ipv4_addr -> bool

  val ipv4_addr_gt : ipv4_addr -> ipv4_addr -> bool

  val ipv4_addr_lt : ipv4_addr -> ipv4_addr -> bool

  val ipv4_addr_ge : ipv4_addr -> ipv4_addr -> bool

  val ipv4_addr_le : ipv4_addr -> ipv4_addr -> bool

  val ipv6_addr_eq : ipv6_addr -> ipv6_addr -> bool

  val ipv6_addr_gt : ipv6_addr -> ipv6_addr -> bool

  val ipv6_addr_lt : ipv6_addr -> ipv6_addr -> bool

  val ipv6_addr_ge : ipv6_addr -> ipv6_addr -> bool

  val ipv6_addr_le : ipv6_addr -> ipv6_addr -> bool

  val tcp_port_eq : tcp_port -> tcp_port -> bool

  val tcp_port_gt : tcp_port -> tcp_port -> bool

  val tcp_port_lt : tcp_port -> tcp_port -> bool

  val tcp_port_ge : tcp_port -> tcp_port -> bool

  val tcp_port_le : tcp_port -> tcp_port -> bool

  val udp_port_eq : udp_port -> udp_port -> bool

  val udp_port_gt : udp_port -> udp_port -> bool

  val udp_port_lt : udp_port -> udp_port -> bool

  val udp_port_ge : udp_port -> udp_port -> bool

  val udp_port_le : udp_port -> udp_port -> bool
end

module Make (B : B) :
  S
  with type netif := B.netif
   and type ether_addr := B.Ether.ether_addr
   and type ipv4_addr := B.IPv4.ipv4_addr
   and type ipv6_addr := B.IPv6.ipv6_addr
   and type tcp_port := B.tcp_port
   and type udp_port := B.udp_port
   and type rlu_ipv4 := B.rlu_ipv4
   and type rlu_ipv6 := B.rlu_ipv6
   and type conn_state := B.conn_state
   and type conn_tracker := B.conn_tracker
   and type pdu := B.PDU.pdu = struct
  include B
  open Ether
  open IPv4
  open IPv6
  open TCP
  open UDP
  open PDU

  (*$ let words =
        [ "ether_addr"
        ; "ipv4_addr"
        ; "ipv6_addr"
        ]
      in

      let ops =
        [ "eq", "="
        ; "gt", ">"
        ; "lt", "<"
        ; "ge", ">="
        ; "le", "<="
        ]
      in

      List.iter
        (fun name ->
           List.iter
             (fun (op1, op2) ->
                Printf.printf {|
                    let %s_%s a b = compare_%s a b %s 0
                  |}
                  name op1 name op2
             )
             ops
        )
        words;

      let words =
        [ "tcp_port"
        ; "udp_port"
        ]
      in

      List.iter
        (fun name ->
           List.iter
             (fun (op1, op2) ->
                Printf.printf {|
                    let %s_%s a b = compare a b %s 0
                  |}
                  name op1 op2
             )
             ops
        )
        words
  *)
  let ether_addr_eq a b = compare_ether_addr a b = 0

  let ether_addr_gt a b = compare_ether_addr a b > 0

  let ether_addr_lt a b = compare_ether_addr a b < 0

  let ether_addr_ge a b = compare_ether_addr a b >= 0

  let ether_addr_le a b = compare_ether_addr a b <= 0

  let ipv4_addr_eq a b = compare_ipv4_addr a b = 0

  let ipv4_addr_gt a b = compare_ipv4_addr a b > 0

  let ipv4_addr_lt a b = compare_ipv4_addr a b < 0

  let ipv4_addr_ge a b = compare_ipv4_addr a b >= 0

  let ipv4_addr_le a b = compare_ipv4_addr a b <= 0

  let ipv6_addr_eq a b = compare_ipv6_addr a b = 0

  let ipv6_addr_gt a b = compare_ipv6_addr a b > 0

  let ipv6_addr_lt a b = compare_ipv6_addr a b < 0

  let ipv6_addr_ge a b = compare_ipv6_addr a b >= 0

  let ipv6_addr_le a b = compare_ipv6_addr a b <= 0

  let tcp_port_eq a b = compare a b = 0

  let tcp_port_gt a b = compare a b > 0

  let tcp_port_lt a b = compare a b < 0

  let tcp_port_ge a b = compare a b >= 0

  let tcp_port_le a b = compare a b <= 0

  let udp_port_eq a b = compare a b = 0

  let udp_port_gt a b = compare a b > 0

  let udp_port_lt a b = compare a b < 0

  let udp_port_ge a b = compare a b >= 0

  let udp_port_le a b = compare a b <= 0

  (*$*)

  (*$ print_type_pred_def ()
  *)
  type pred =
    | True
    | False
    | And of pred * pred
    | Or of pred * pred
    | Not of pred
    | Guide_decap_to_layer3 of pred
    | Guide_decap_to_layer4 of pred
    (* Ethernet *)
    | Contains_Ether
    (* Ethernet - src *)
    | Ether_src_addr_one_of of ether_addr array
    | Ether_src_addr_eq of ether_addr
    | Ether_src_addr_gt of ether_addr
    | Ether_src_addr_lt of ether_addr
    | Ether_src_addr_ge of ether_addr
    | Ether_src_addr_le of ether_addr
    (* Ethernet - dst *)
    | Ether_dst_addr_one_of of ether_addr array
    | Ether_dst_addr_eq of ether_addr
    | Ether_dst_addr_gt of ether_addr
    | Ether_dst_addr_lt of ether_addr
    | Ether_dst_addr_ge of ether_addr
    | Ether_dst_addr_le of ether_addr
    (* IPv4 *)
    | Contains_IPv4
    (* IPv4 - src *)
    | IPv4_src_addr_one_of of ipv4_addr array
    | IPv4_src_addr_eq of ipv4_addr
    | IPv4_src_addr_gt of ipv4_addr
    | IPv4_src_addr_lt of ipv4_addr
    | IPv4_src_addr_ge of ipv4_addr
    | IPv4_src_addr_le of ipv4_addr
    (* IPv4 - dst *)
    | IPv4_dst_addr_one_of of ipv4_addr array
    | IPv4_dst_addr_eq of ipv4_addr
    | IPv4_dst_addr_gt of ipv4_addr
    | IPv4_dst_addr_lt of ipv4_addr
    | IPv4_dst_addr_ge of ipv4_addr
    | IPv4_dst_addr_le of ipv4_addr
    (* IPv6 *)
    | Contains_IPv6
    (* IPv6 - src *)
    | IPv6_src_addr_one_of of ipv6_addr array
    | IPv6_src_addr_eq of ipv6_addr
    | IPv6_src_addr_gt of ipv6_addr
    | IPv6_src_addr_lt of ipv6_addr
    | IPv6_src_addr_ge of ipv6_addr
    | IPv6_src_addr_le of ipv6_addr
    (* IPv6 - dst *)
    | IPv6_dst_addr_one_of of ipv6_addr array
    | IPv6_dst_addr_eq of ipv6_addr
    | IPv6_dst_addr_gt of ipv6_addr
    | IPv6_dst_addr_lt of ipv6_addr
    | IPv6_dst_addr_ge of ipv6_addr
    | IPv6_dst_addr_le of ipv6_addr
    (* TCP *)
    | Contains_TCP
    (* TCP - src *)
    | TCP_src_port_one_of of tcp_port array
    | TCP_src_port_eq of tcp_port
    | TCP_src_port_gt of tcp_port
    | TCP_src_port_lt of tcp_port
    | TCP_src_port_ge of tcp_port
    | TCP_src_port_le of tcp_port
    (* TCP - dst *)
    | TCP_dst_port_one_of of tcp_port array
    | TCP_dst_port_eq of tcp_port
    | TCP_dst_port_gt of tcp_port
    | TCP_dst_port_lt of tcp_port
    | TCP_dst_port_ge of tcp_port
    | TCP_dst_port_le of tcp_port
    (* UDP *)
    | Contains_UDP
    (* UDP - src *)
    | UDP_src_port_one_of of udp_port array
    | UDP_src_port_eq of udp_port
    | UDP_src_port_gt of udp_port
    | UDP_src_port_lt of udp_port
    | UDP_src_port_ge of udp_port
    | UDP_src_port_le of udp_port
    (* UDP - dst *)
    | UDP_dst_port_one_of of udp_port array
    | UDP_dst_port_eq of udp_port
    | UDP_dst_port_gt of udp_port
    | UDP_dst_port_lt of udp_port
    | UDP_dst_port_ge of udp_port
    | UDP_dst_port_le of udp_port
    (* ICMPv4 *)
    | Contains_ICMPv4
    | ICMPv4_ty_eq_Echo_reply
    | ICMPv4_ty_eq_Destination_unreachable
    | ICMPv4_ty_eq_Source_quench
    | ICMPv4_ty_eq_Redirect
    | ICMPv4_ty_eq_Echo_request
    | ICMPv4_ty_eq_Time_exceeded
    | ICMPv4_ty_eq_Parameter_problem
    | ICMPv4_ty_eq_Timestamp_request
    | ICMPv4_ty_eq_Timestamp_reply
    | ICMPv4_ty_eq_Information_request
    | ICMPv4_ty_eq_Information_reply
    (* connection tracking *)
    | Conn_state_eq of {tracker : conn_tracker; target_state : conn_state}
    (* custom *)
    | Custom of (netif -> rlu_ipv4 -> rlu_ipv6 -> pdu -> bool)

  (*$*)

  (*$ let words =
    [ "Ethernet", "addr", "Ether", "ether", "Ether_frame", "ether_frame"
    ; "IPv4", "addr", "IPv4", "ipv4", "IPv4_pkt", "ipv4_pkt"
    ; "IPv6", "addr", "IPv6", "ipv6", "IPv6_pkt", "ipv6_pkt"
    ; "TCP", "port", "TCP", "tcp", "TCP_pdu", "tcp_pdu"
    ; "UDP", "port", "UDP", "udp", "UDP_pdu", "udp_pdu"
    ]
    in

    let ops =
    [ "eq"
    ; "gt"
    ; "lt"
    ; "ge"
    ; "le"
    ]
    in

    print_endline {|
        let eval_pred (pred : pred) ~(src_netif : netif) (rlu_ipv4 : rlu_ipv4)
            (rlu_ipv6 : rlu_ipv6) (pdu : pdu) : bool =
          let rec aux (pred : pred) (src_netif : netif) (rlu_ipv4 : rlu_ipv4)
              (rlu_ipv6 : rlu_ipv6) (pdu : pdu) : bool =
            match pred with
            | True -> true
            | False -> false
            | And (p1, p2) ->
              aux p1 src_netif rlu_ipv4 rlu_ipv6 pdu
              && aux p2 src_netif rlu_ipv4 rlu_ipv6 pdu
            | Or (p1, p2) ->
              aux p1 src_netif rlu_ipv4 rlu_ipv6 pdu
              || aux p2 src_netif rlu_ipv4 rlu_ipv6 pdu
            | Not p -> not (aux p src_netif rlu_ipv4 rlu_ipv6 pdu)
            | Guide_decap_to_layer3 pred -> (
                match PDU_to.layer3_pdu pdu with
                | Some pdu -> aux pred src_netif rlu_ipv4 rlu_ipv6 (Layer3 pdu)
                | None -> aux pred src_netif rlu_ipv4 rlu_ipv6 pdu )
            | Guide_decap_to_layer4 pred -> (
                match PDU_to.layer4_pdu pdu with
                | Some pdu -> aux pred src_netif rlu_ipv4 rlu_ipv6 (Layer4 pdu)
                | None ->
                  aux pred src_netif rlu_ipv4 rlu_ipv6 pdu
              )
      |};

    List.iter
    (fun (comment, field_name, proto1, proto2, pdu_con, pdu_ty) ->
     Printf.printf
       {|
         (* %s *)
         | Contains_%s -> (
           match PDU_to.%s pdu with
           | Some _ -> true
           | None -> false
           )
       |}
       comment
       proto1
       pdu_ty;
     List.iter
       (fun src_dst ->
          Printf.printf "(* %s - %s *)" comment src_dst;
          Printf.printf {|
              | %s_%s_%s_one_of arr -> (
                match PDU_to.%s_header pdu with
                | Some header ->
                  Array.mem (%s_header_to_%s_%s header) arr
                | _ -> false
                )
            |}
            proto1 src_dst field_name
            proto2
            proto2 src_dst field_name;
          List.iter
            (fun op ->
               Printf.printf {|
                  | %s_%s_%s_%s x -> (
                    match PDU_to.%s_header pdu with
                    | Some header ->
                      %s_%s_%s (%s_header_to_%s_%s header) x
                    | _ -> false
                    )
               |}
               proto1 src_dst field_name op
               proto2
               proto2 field_name op proto2 src_dst field_name
            )
          ops
       )
       [ "src"; "dst" ]
    )
    words;

    print_endline {|
      (* ICMPv4 *)
      | Contains_ICMPv4 -> (
          match PDU_to.icmpv4_pkt pdu with
          | Some _ -> true
          | None -> false
        )
      |};

    List.iter
      (fun (ty, has_value) ->
        Printf.printf {|
          | ICMPv4_ty_eq_%s -> (
              match PDU_to.icmpv4_header pdu with
              | None -> false
              | Some h ->
                let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
                match ty with
                | ICMPv4_%s %s -> true
                | _ -> false
            )
         |}
         ty
         ty (if has_value then "_" else "")
      )
      [ "Echo_reply", true
      ; "Destination_unreachable", false
      ; "Source_quench", false
      ; "Redirect", false
      ; "Echo_request", true
      ; "Time_exceeded", false
      ; "Parameter_problem", false
      ; "Timestamp_request", true
      ; "Timestamp_reply", true
      ; "Information_request", true
      ; "Information_reply", true
      ];

    print_endline {|
      (* connection tracking *)
      | Conn_state_eq {tracker; target_state} ->
        lookup_conn_state ~reject_new:false ~no_update:true tracker pdu
        = target_state
      (* custom *)
      | Custom pred -> pred src_netif rlu_ipv4 rlu_ipv6 pdu
    in
    aux pred src_netif rlu_ipv4 rlu_ipv6 pdu
      |}
  *)
  let eval_pred (pred : pred) ~(src_netif : netif) (rlu_ipv4 : rlu_ipv4)
      (rlu_ipv6 : rlu_ipv6) (pdu : pdu) : bool =
    let rec aux (pred : pred) (src_netif : netif) (rlu_ipv4 : rlu_ipv4)
        (rlu_ipv6 : rlu_ipv6) (pdu : pdu) : bool =
      match pred with
      | True ->
        true
      | False ->
        false
      | And (p1, p2) ->
        aux p1 src_netif rlu_ipv4 rlu_ipv6 pdu
        && aux p2 src_netif rlu_ipv4 rlu_ipv6 pdu
      | Or (p1, p2) ->
        aux p1 src_netif rlu_ipv4 rlu_ipv6 pdu
        || aux p2 src_netif rlu_ipv4 rlu_ipv6 pdu
      | Not p ->
        not (aux p src_netif rlu_ipv4 rlu_ipv6 pdu)
      | Guide_decap_to_layer3 pred -> (
          match PDU_to.layer3_pdu pdu with
          | Some pdu ->
            aux pred src_netif rlu_ipv4 rlu_ipv6 (Layer3 pdu)
          | None ->
            aux pred src_netif rlu_ipv4 rlu_ipv6 pdu )
      | Guide_decap_to_layer4 pred -> (
          match PDU_to.layer4_pdu pdu with
          | Some pdu ->
            aux pred src_netif rlu_ipv4 rlu_ipv6 (Layer4 pdu)
          | None ->
            aux pred src_netif rlu_ipv4 rlu_ipv6 pdu )
      (* Ethernet *)
      | Contains_Ether -> (
          match PDU_to.ether_frame pdu with Some _ -> true | None -> false )
      (* Ethernet - src *)
      | Ether_src_addr_one_of arr -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            Array.mem (ether_header_to_src_addr header) arr
          | _ ->
            false )
      | Ether_src_addr_eq x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_eq (ether_header_to_src_addr header) x
          | _ ->
            false )
      | Ether_src_addr_gt x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_gt (ether_header_to_src_addr header) x
          | _ ->
            false )
      | Ether_src_addr_lt x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_lt (ether_header_to_src_addr header) x
          | _ ->
            false )
      | Ether_src_addr_ge x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_ge (ether_header_to_src_addr header) x
          | _ ->
            false )
      | Ether_src_addr_le x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_le (ether_header_to_src_addr header) x
          | _ ->
            false )
      (* Ethernet - dst *)
      | Ether_dst_addr_one_of arr -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            Array.mem (ether_header_to_dst_addr header) arr
          | _ ->
            false )
      | Ether_dst_addr_eq x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_eq (ether_header_to_dst_addr header) x
          | _ ->
            false )
      | Ether_dst_addr_gt x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_gt (ether_header_to_dst_addr header) x
          | _ ->
            false )
      | Ether_dst_addr_lt x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_lt (ether_header_to_dst_addr header) x
          | _ ->
            false )
      | Ether_dst_addr_ge x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_ge (ether_header_to_dst_addr header) x
          | _ ->
            false )
      | Ether_dst_addr_le x -> (
          match PDU_to.ether_header pdu with
          | Some header ->
            ether_addr_le (ether_header_to_dst_addr header) x
          | _ ->
            false )
      (* IPv4 *)
      | Contains_IPv4 -> (
          match PDU_to.ipv4_pkt pdu with Some _ -> true | None -> false )
      (* IPv4 - src *)
      | IPv4_src_addr_one_of arr -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            Array.mem (ipv4_header_to_src_addr header) arr
          | _ ->
            false )
      | IPv4_src_addr_eq x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_eq (ipv4_header_to_src_addr header) x
          | _ ->
            false )
      | IPv4_src_addr_gt x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_gt (ipv4_header_to_src_addr header) x
          | _ ->
            false )
      | IPv4_src_addr_lt x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_lt (ipv4_header_to_src_addr header) x
          | _ ->
            false )
      | IPv4_src_addr_ge x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_ge (ipv4_header_to_src_addr header) x
          | _ ->
            false )
      | IPv4_src_addr_le x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_le (ipv4_header_to_src_addr header) x
          | _ ->
            false )
      (* IPv4 - dst *)
      | IPv4_dst_addr_one_of arr -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            Array.mem (ipv4_header_to_dst_addr header) arr
          | _ ->
            false )
      | IPv4_dst_addr_eq x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_eq (ipv4_header_to_dst_addr header) x
          | _ ->
            false )
      | IPv4_dst_addr_gt x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_gt (ipv4_header_to_dst_addr header) x
          | _ ->
            false )
      | IPv4_dst_addr_lt x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_lt (ipv4_header_to_dst_addr header) x
          | _ ->
            false )
      | IPv4_dst_addr_ge x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_ge (ipv4_header_to_dst_addr header) x
          | _ ->
            false )
      | IPv4_dst_addr_le x -> (
          match PDU_to.ipv4_header pdu with
          | Some header ->
            ipv4_addr_le (ipv4_header_to_dst_addr header) x
          | _ ->
            false )
      (* IPv6 *)
      | Contains_IPv6 -> (
          match PDU_to.ipv6_pkt pdu with Some _ -> true | None -> false )
      (* IPv6 - src *)
      | IPv6_src_addr_one_of arr -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            Array.mem (ipv6_header_to_src_addr header) arr
          | _ ->
            false )
      | IPv6_src_addr_eq x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_eq (ipv6_header_to_src_addr header) x
          | _ ->
            false )
      | IPv6_src_addr_gt x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_gt (ipv6_header_to_src_addr header) x
          | _ ->
            false )
      | IPv6_src_addr_lt x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_lt (ipv6_header_to_src_addr header) x
          | _ ->
            false )
      | IPv6_src_addr_ge x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_ge (ipv6_header_to_src_addr header) x
          | _ ->
            false )
      | IPv6_src_addr_le x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_le (ipv6_header_to_src_addr header) x
          | _ ->
            false )
      (* IPv6 - dst *)
      | IPv6_dst_addr_one_of arr -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            Array.mem (ipv6_header_to_dst_addr header) arr
          | _ ->
            false )
      | IPv6_dst_addr_eq x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_eq (ipv6_header_to_dst_addr header) x
          | _ ->
            false )
      | IPv6_dst_addr_gt x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_gt (ipv6_header_to_dst_addr header) x
          | _ ->
            false )
      | IPv6_dst_addr_lt x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_lt (ipv6_header_to_dst_addr header) x
          | _ ->
            false )
      | IPv6_dst_addr_ge x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_ge (ipv6_header_to_dst_addr header) x
          | _ ->
            false )
      | IPv6_dst_addr_le x -> (
          match PDU_to.ipv6_header pdu with
          | Some header ->
            ipv6_addr_le (ipv6_header_to_dst_addr header) x
          | _ ->
            false )
      (* TCP *)
      | Contains_TCP -> (
          match PDU_to.tcp_pdu pdu with Some _ -> true | None -> false )
      (* TCP - src *)
      | TCP_src_port_one_of arr -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            Array.mem (tcp_header_to_src_port header) arr
          | _ ->
            false )
      | TCP_src_port_eq x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_eq (tcp_header_to_src_port header) x
          | _ ->
            false )
      | TCP_src_port_gt x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_gt (tcp_header_to_src_port header) x
          | _ ->
            false )
      | TCP_src_port_lt x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_lt (tcp_header_to_src_port header) x
          | _ ->
            false )
      | TCP_src_port_ge x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_ge (tcp_header_to_src_port header) x
          | _ ->
            false )
      | TCP_src_port_le x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_le (tcp_header_to_src_port header) x
          | _ ->
            false )
      (* TCP - dst *)
      | TCP_dst_port_one_of arr -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            Array.mem (tcp_header_to_dst_port header) arr
          | _ ->
            false )
      | TCP_dst_port_eq x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_eq (tcp_header_to_dst_port header) x
          | _ ->
            false )
      | TCP_dst_port_gt x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_gt (tcp_header_to_dst_port header) x
          | _ ->
            false )
      | TCP_dst_port_lt x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_lt (tcp_header_to_dst_port header) x
          | _ ->
            false )
      | TCP_dst_port_ge x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_ge (tcp_header_to_dst_port header) x
          | _ ->
            false )
      | TCP_dst_port_le x -> (
          match PDU_to.tcp_header pdu with
          | Some header ->
            tcp_port_le (tcp_header_to_dst_port header) x
          | _ ->
            false )
      (* UDP *)
      | Contains_UDP -> (
          match PDU_to.udp_pdu pdu with Some _ -> true | None -> false )
      (* UDP - src *)
      | UDP_src_port_one_of arr -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            Array.mem (udp_header_to_src_port header) arr
          | _ ->
            false )
      | UDP_src_port_eq x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_eq (udp_header_to_src_port header) x
          | _ ->
            false )
      | UDP_src_port_gt x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_gt (udp_header_to_src_port header) x
          | _ ->
            false )
      | UDP_src_port_lt x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_lt (udp_header_to_src_port header) x
          | _ ->
            false )
      | UDP_src_port_ge x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_ge (udp_header_to_src_port header) x
          | _ ->
            false )
      | UDP_src_port_le x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_le (udp_header_to_src_port header) x
          | _ ->
            false )
      (* UDP - dst *)
      | UDP_dst_port_one_of arr -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            Array.mem (udp_header_to_dst_port header) arr
          | _ ->
            false )
      | UDP_dst_port_eq x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_eq (udp_header_to_dst_port header) x
          | _ ->
            false )
      | UDP_dst_port_gt x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_gt (udp_header_to_dst_port header) x
          | _ ->
            false )
      | UDP_dst_port_lt x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_lt (udp_header_to_dst_port header) x
          | _ ->
            false )
      | UDP_dst_port_ge x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_ge (udp_header_to_dst_port header) x
          | _ ->
            false )
      | UDP_dst_port_le x -> (
          match PDU_to.udp_header pdu with
          | Some header ->
            udp_port_le (udp_header_to_dst_port header) x
          | _ ->
            false )
      (* ICMPv4 *)
      | Contains_ICMPv4 -> (
          match PDU_to.icmpv4_pkt pdu with Some _ -> true | None -> false )
      | ICMPv4_ty_eq_Echo_reply -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Echo_reply _ -> true | _ -> false ) )
      | ICMPv4_ty_eq_Destination_unreachable -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Destination_unreachable -> true | _ -> false )
        )
      | ICMPv4_ty_eq_Source_quench -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Source_quench -> true | _ -> false ) )
      | ICMPv4_ty_eq_Redirect -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Redirect -> true | _ -> false ) )
      | ICMPv4_ty_eq_Echo_request -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Echo_request _ -> true | _ -> false ) )
      | ICMPv4_ty_eq_Time_exceeded -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Time_exceeded -> true | _ -> false ) )
      | ICMPv4_ty_eq_Parameter_problem -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Parameter_problem -> true | _ -> false ) )
      | ICMPv4_ty_eq_Timestamp_request -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Timestamp_request _ -> true | _ -> false ) )
      | ICMPv4_ty_eq_Timestamp_reply -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Timestamp_reply _ -> true | _ -> false ) )
      | ICMPv4_ty_eq_Information_request -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Information_request _ -> true | _ -> false ) )
      | ICMPv4_ty_eq_Information_reply -> (
          match PDU_to.icmpv4_header pdu with
          | None ->
            false
          | Some h -> (
              let ty = ICMPv4.icmpv4_header_to_icmpv4_type h in
              match ty with ICMPv4_Information_reply _ -> true | _ -> false ) )
      (* connection tracking *)
      | Conn_state_eq {tracker; target_state} ->
        lookup_conn_state ~reject_new:false ~no_update:true tracker pdu
        = target_state
      (* custom *)
      | Custom pred ->
        pred src_netif rlu_ipv4 rlu_ipv6 pdu
    in
    aux pred src_netif rlu_ipv4 rlu_ipv6 pdu

  (*$*)
end
