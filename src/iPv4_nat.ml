module type B = sig
  include Tree.S

  open PDU

  type l4_port

  type l4_header

  type l4_header_to_src_port

  type l4_header_to_dst_port

  val pdu_to_l4_header : pdu -> l4_header option

  val l4_header_to_src_port : l4_header -> l4_port

  val l4_header_to_dst_port : l4_header -> l4_port

  val pdu_map_l4_header : (l4_header -> l4_header) -> pdu -> pdu

  val make_l4_header : src_port:l4_port -> dst_port:l4_port -> l4_header
end

module Make (B : B) = struct
  include B
  open IPv4

  (* let make_nat_static_single_src_to_single_src
   *     (mapping : (ipv4_addr * ipv4_addr) list) ~(okay : decision_tree_branch)
   *     ~(failed : decision_tree_branch) : selecting_modifier =
   *   let src_table = Misc_utils.alist_to_hashtbl mapping in
   *   let dst_table = Misc_utils.(alist_to_hashtbl (swap_alist mapping)) in
   *   let okay_choice = 0 in
   *   let failed_choice = 1 in
   *   let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 pdu _choices =
   *     match PDU_to.ipv4_header pdu with
   *     | None ->
   *       Some (failed_choice, pdu)
   *     | Some header -> (
   *         let src_addr = ipv4_header_to_src_addr header in
   *         let dst_addr = ipv4_header_to_dst_addr header in
   *         match
   *           ( Hashtbl.find_opt src_table src_addr
   *           , Hashtbl.find_opt dst_table dst_addr )
   *         with
   *         | None, None | Some _, Some _ ->
   *           Some (failed_choice, pdu)
   *         | Some src_addr, None ->
   *           Some
   *             ( okay_choice
   *             , PDU_map.ipv4_header
   *                 (fun _ -> make_ipv4_header ~src_addr ~dst_addr)
   *                 pdu )
   *         | None, Some dst_addr ->
   *           Some
   *             ( okay_choice
   *             , PDU_map.ipv4_header
   *                 (fun _ -> make_ipv4_header ~src_addr ~dst_addr)
   *                 pdu ) )
   *   in
   *   Selecting_modifier {logic_unit; choices = [|okay; failed|]}
   * 
   * let make_nat_static_single_dst_to_single_dst
   *     (mapping : (ipv4_addr * ipv4_addr) list) ~(okay : decision_tree_branch)
   *     ~(failed : decision_tree_branch) : selecting_modifier =
   *   make_nat_static_single_src_to_single_src
   *     (Misc_utils.swap_alist mapping)
   *     ~okay ~failed
   * 
   * let make_nat_static_single_src_addr_tcp_port_to_single_src_addr_tcp_port
   *     (mapping : ((ipv4_addr * l4_port) * (ipv4_addr * l4_port)) list)
   *     ~(okay : decision_tree_branch) ~(failed : decision_tree_branch) :
   *   selecting_modifier =
   *   let src_table = Misc_utils.alist_to_hashtbl mapping in
   *   let dst_table = Misc_utils.(alist_to_hashtbl (swap_alist mapping)) in
   *   let okay_choice = 0 in
   *   let failed_choice = 1 in
   *   let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 pdu _choices =
   *     match (PDU_to.ipv4_header pdu, pdu_to_l4_header pdu) with
   *     | Some ipv4_header, Some l4_header -> (
   *         let src_addr = ipv4_header_to_src_addr ipv4_header in
   *         let dst_addr = ipv4_header_to_dst_addr ipv4_header in
   *         let src_port = l4_header_to_src_port l4_header in
   *         let dst_port = l4_header_to_dst_port l4_header in
   *         match
   *           ( Hashtbl.find_opt src_table (src_addr, src_port)
   *           , Hashtbl.find_opt dst_table (dst_addr, dst_port) )
   *         with
   *         | None, None | Some _, Some _ ->
   *           Some (failed_choice, pdu)
   *         | Some (src_addr, src_port), None ->
   *           Some
   *             ( okay_choice
   *             , pdu
   *               |> PDU_map.ipv4_header (fun _ ->
   *                   make_ipv4_header ~src_addr ~dst_addr )
   *               |> pdu_map_l4_header (fun _ ->
   *                   make_l4_header ~src_port ~dst_port ) )
   *         | None, Some (dst_addr, dst_port) ->
   *           Some
   *             ( okay_choice
   *             , pdu
   *               |> PDU_map.ipv4_header (fun _ ->
   *                   make_ipv4_header ~src_addr ~dst_addr )
   *               |> pdu_map_l4_header (fun _ ->
   *                   make_l4_header ~src_port ~dst_port ) ) )
   *     | _ ->
   *       Some (failed_choice, pdu)
   *   in
   *   Selecting_modifier {logic_unit; choices = [|okay; failed|]}
   * 
   * let make_nat_static_single_dst_addr_tcp_port_to_single_dst_addr_tcp_port
   *     (mapping : ((ipv4_addr * l4_port) * (ipv4_addr * l4_port)) list)
   *     ~(okay : decision_tree_branch) ~(failed : decision_tree_branch) :
   *   selecting_modifier =
   *   make_nat_static_single_src_addr_tcp_port_to_single_src_addr_tcp_port
   *     (Misc_utils.swap_alist mapping)
   *     ~okay ~failed
   * 
   * let make_nat_dynamic_multi_src_addr_multi_port_to_single_src_addr_multi_port
   *     ~(max_conn : int) ~(internal_src_addr : ipv4_addr)
   *     ~(external_src_addr : ipv4_addr)
   *     ~(external_src_addr_mask : ipv4_addr subnet_mask)
   *     ~(okay : decision_tree_branch) ~(failed : decision_tree_branch) :
   *   selecting_modifier =
   *   let okay_choice = 0 in
   *   let failed_choice = 1 in
   *   let init_size = max_conn / 2 in
   *   let table_in_to_ext =
   *     Lookup_table.create ~timeout_ms:None ~max_size:max_conn ~init_size
   *   in
   *   let table_ext_to_int =
   *     Lookup_table.create ~timeout_ms:None ~max_size:max_conn ~init_size
   *   in
   *   let conn_tracker =
   *     Conn_track.make ~max_conn ~init_size ~timeout_ms:30_000L
   *   in
   *   let internal_ports_used = Hashset.create 5000 in
   *   let external_ports_used = Hashset.create 5000 in
   *   let internal_port_selector =
   *     Misc_utils.make_random_generator_exc_opt
   *       ~pred:(fun x -> not (Hashset.mem internal_ports_used x))
   *       ~from:1000 4000
   *   in
   *   let external_port_selector =
   *     Misc_utils.make_random_generator_exc_opt
   *       ~pred:(fun x -> not (Hashset.mem external_ports_used x))
   *       ~from:1000 4000
   *   in
   *   let external_src_net_addr =
   *     ipv4_addr_to_net_addr ~addr:external_src_addr
   *       ~mask:external_src_addr_mask
   *   in
   *   let logic_unit ~src_netif:_ _rlu_ipv4 _rlu_ipv6 pdu _choices =
   *     match PDU_to.ipv4_header pdu with
   *     | None ->
   *       Some (failed_choice, pdu)
   *     | Some ingress_ipv4_header ->
   *       let ingress_src_addr = ipv4_header_to_src_addr ingress_ipv4_header in
   *       let ingress_dst_addr = ipv4_header_to_dst_addr ingress_ipv4_header in
   *       let ingress_src_net_addr =
   *         ipv4_addr_to_net_addr ~addr:ingress_src_addr
   *           ~mask:external_src_addr_mask
   *       in
   *       if ingress_dst_addr = external_src_addr then
   *         (\* map back to internal network
   * 
   *            src ip:port -> self internal ip:random port
   *            dst ip:port -> recorded internal ip:port
   *         *\)
   *         match
   *           Conn_track.lookup_conn_state ~reject_new:true conn_tracker pdu
   *         with
   *         | Established ->
   *           (\* let pdu =
   *            *   pdu
   *            *   |> pdu_map_ipv4_header (fun header ->
   *            *       make_header
   *            *     )
   *            * in *\)
   *           Some (okay_choice, pdu)
   *         | _ ->
   *           Some (failed_choice, pdu)
   *       else if ingress_src_net_addr = external_src_net_addr then
   *         Some (failed_choice, pdu)
   *       else
   *         (\* map to external ip address and port
   * 
   *            src ip:port -> self external ip:random port
   *            dst ip:port -> unchanged
   *         *\)
   *         Some (okay_choice, pdu)
   *   in
   *   Selecting_modifier {logic_unit; choices = [|okay; failed|]} *)
end
