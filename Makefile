SRCFILES = src/*.ml* \
	test-src/*.ml \
	tests/*.ml*

CINAPSFILES = src/*.cinaps \
	test-src/*.cinaps

OCAMLFORMAT = ocamlformat \
	--inplace \
	--field-space loose \
	--let-and sparse \
	--let-open auto \
	--type-decl sparse \
	--sequence-style terminator \
	$(SRCFILES) \
	$(CINAPSFILES)

OCPINDENT = ocp-indent \
	--inplace \
	$(SRCFILES) \
	$(CINAPSFILES)

.PHONY: all
all : lib test-lib doc

.PHONY: lib
lib :
	dune build -p firewall-tree

.PHONY: test-lib
test-lib :
	dune build -p firewall-tree-test

.PHONY: format
format :
	$(OCAMLFORMAT)
	$(OCPINDENT)

.PHONY: cinaps
cinaps :
	cinaps -i $(SRCFILES)
	$(OCAMLFORMAT)
	$(OCPINDENT)

.PHONY: doc
doc : clean_doc
	dune build @doc
	cp -r _build/default/_doc .

.PHONY: browse_doc
browse_doc : doc
	$$BROWSER _doc/_html/index.html

.PHONY: test
test :
	dune runtest --force

.PHONY: coverage
coverage : clean_coverage
	BISECT_ENABLE=YES dune runtest --force
	bisect-ppx-report -I _build/default/ -html _coverage/ \
		`find . -name 'bisect*.out'`

.PHONY: browse_coverage
browse_coverage : coverage
	$$BROWSER _coverage/index.html

.PHONY : clean
clean: clean_doc clean_coverage
	dune clean

.PHONY : clean_doc
clean_doc:
	rm -rf _doc

.PHONY : clean_coverage
clean_coverage :
	rm -f `find . -name 'bisect*.out'`
	rm -rf _coverage
